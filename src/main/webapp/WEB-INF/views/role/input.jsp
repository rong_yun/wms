<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>信息管理系统</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
<link href="/style/common_style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/js/plugins/artDialog/jquery.artDialog.js?skin=blue"></script>
<script type="text/javascript" src="/js/plugins/jqueryFrom/jQueryForm.js"></script>
<script type="text/javascript" src="/js/commonAll.js"></script>
	<script>
		$(function () {
            //提示弹框
			$("#editForm").ajaxForm(function (data) {
				showDialog("保存成功",function () {
					location.href="/role/list.do";
                });
            });
            //表单提交选中所有
            $(".btn_submit").click(function () {
                //选中右边所有的option
                $(".selected_permission option").prop("selected",true);
                $(".selected_menu option").prop("selected",true);
                //提交表单
                $("#editForm").submit();
            });
        });
        //文档加载完毕执行函数
        $(function () {
            //左移右移
            //选中右移
            $("#selectmenu").click(function () {
                $(".all_menu option:selected").appendTo(".selected_menu");
            });
            //全部右移
            $("#selectmenuAll").click(function () {
                $(".all_menu option").appendTo(".selected_menu");
            });
            //选中左移
            $("#deselectmenu").click(function () {
                $(".selected_menu option:selected").appendTo(".all_menu");
            });
            //全部左移
            $("#deselectmenuAll").click(function () {
                $(".selected_menu option").appendTo(".all_menu");
            });

            //消除重复
            var arr=$.map($(".selected_menu option"),function (ele) {
                //获取到option的value值
                return ele.value;
            });
            //遍历数组获取元素的value值
            $.each(arr,function (index,val) {
                //遍历左边的option
                $.each($(".all_menu option"),function (i,ele) {
                    //判断是否重复
                    if(val==ele.value){
                        //重复删除
                        $(ele).remove();
                    }
                });
            });
        });
	</script>
</head>
<body>
<form id="editForm" action="/role/saveOrUpdate.do" method="post">
	<input type="hidden" name="id" value="${entity.id}">
	<div id="container">
		<div id="nav_links">
			<span style="color: #1A5CC6;">用户编辑</span>
			<div id="page_close">
				<a>
					<img src="/images/common/page_close.png" width="20" height="20" style="vertical-align: text-top;"/>
				</a>
			</div>
		</div>
		<div class="ui_content">
			<table cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
				<tr>
					<td class="ui_text_rt" width="140">角色名称</td>
					<td class="ui_text_lt">
						<input name="name" value="${entity.name}" class="ui_input_txt02"/>
					</td>
				</tr>
				<tr>
					<td class="ui_text_rt" width="140">角色编码</td>
					<td class="ui_text_lt">
						<input name="sn" value="${entity.sn}" id="password" class="ui_input_txt02"/>
					</td>
				</tr>
				<tr>
					<td class="ui_text_rt" width="140">分配权限</td>
					<td class="ui_text_lt">
						<table>
							<tr>
								<td>
									<select multiple="true" class="ui_multiselect01 all_permission" >
										<c:forEach var="ele" items="${permissions}">
											<option value="${ele.id}">${ele.name}</option>
										</c:forEach>
									</select>
								</td>
								<td align="center">
									<input type="button" id="select" value="-->" class="left2right"/><br/>
									<input type="button" id="selectAll" value="==>" class="left2right"/><br/>
									<input type="button" id="deselect" value="<--" class="left2right"/><br/>
									<input type="button" id="deselectAll" value="<==" class="left2right"/>
								</td>
								<td>
									<select name="ids" multiple="true" class="ui_multiselect01 selected_permission">
										<c:forEach var="ele" items="${entity.permissions}">
											<option value="${ele.id}">${ele.name}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							<%--菜单分配--%>
							<tr>
								<td>
									<select multiple="true" class="ui_multiselect01 all_menu" >
										<c:forEach var="ele" items="${menus}">
											<option value="${ele.id}">${ele.name}</option>
										</c:forEach>
									</select>
								</td>
								<td align="center">
									<input type="button" id="selectmenu" value="-->" class="left2right"/><br/>
									<input type="button" id="selectmenuAll" value="==>" class="left2right"/><br/>
									<input type="button" id="deselectmenu" value="<--" class="left2right"/><br/>
									<input type="button" id="deselectmenuAll" value="<==" class="left2right"/>
								</td>
								<td>
									<select name="menuIds" multiple="true" class="ui_multiselect01 selected_menu">
										<c:forEach var="ele" items="${entity.menus}">
											<option value="${ele.id}">${ele.name}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="ui_text_lt">
						&nbsp;<input type="button" value="确定保存" class="ui_input_btn01 btn_submit" />
						&nbsp;<input id="cancelbutton" type="button" value="重置" class="ui_input_btn01"/>
					</td>
				</tr>
			</table>
		</div>
	</div>
</form>
</body>
</html>