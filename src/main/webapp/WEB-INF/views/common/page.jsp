<%@ page contentType="text/html; charset=UTF-8" %>
<%--分页操作--%>
<div class="ui_tb_h30">
    <div class="ui_flt" style="height: 30px; line-height: 30px;">
        共有
        <span class="ui_txt_bold04">${result.rows}</span>
        条记录，当前第
        <span class="ui_txt_bold04">${result.currentPage}/${result.endPage}</span>
        页
    </div>
    <div class="ui_frt">
        <input type="button" value="首页" class="ui_input_btn01 btn_page" data-page="1"/>
        <input type="button" value="上一页" class="ui_input_btn01 btn_page" data-page="${result.prevPage}"/>
        <input type="button" value="下一页" class="ui_input_btn01 btn_page" data-page="${result.nextPage}"/>
        <input type="button" value="尾页" class="ui_input_btn01 btn_page" data-page="${result.endPage}"/>

        <select name="pageSize" id="pageSize" class="ui_select02">
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="15">15</option>
        </select>
        转到第<input type="text" name="currentPage" value="${qo.currentPage}" class="ui_input_txt01" />页
        <input type="button" class="ui_input_btn01 btn_page" value="跳转"/>
    </div>
</div>
<script>
    //跳转事件
    $(".btn_page").click(function () {
        var page=$(this).data("page")||$(":text[name='currentPage']").val();
        $(":text[name='currentPage']").val(page);
        $("#searchForm").submit();
    });

    //改变页面容量(失去焦点事件)
    $("#pageSize").change(function () {
        //设置当前页
        $(":text[name='currentPage']").val(1);
        //提交表单
        $("#searchForm").submit();
    });
    //回显页面容量
    $("#pageSize option[value='${qo.pageSize}']").prop("selected",true);
</script>