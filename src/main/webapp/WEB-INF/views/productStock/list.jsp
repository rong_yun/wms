<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
<link href="/style/common_style.css" rel="stylesheet" type="text/css">
<link href="/js/plugins/fancyBox/jquery.fancybox.css?v=2.1.5" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/js/plugins/artDialog/jquery.artDialog.js?skin=blue"></script>
<script type="text/javascript" src="/js/plugins/fancyBox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="/js/plugins/fancyBox/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="/js/commonAll.js"></script>
	<script type="text/javascript">
			$(function () {
                $(".fancybox").fancybox();
            });
	</script>
<title>即时库存</title>
<style>
	.alt td{ background:black !important;}
</style>
</head>
<body>
	<form id="searchForm" action="/productStock/list.do" method="post">
		<div id="container">
			<div class="ui_content">
				<div class="ui_text_indent">
					<div id="box_border">
						<div id="box_top">搜索</div>
						<div id="box_center">
							商品名称/编码
							<input type="text" class="ui_input_txt02" name="keyword" value="${qo.keyword}"/>
							仓库
							<select class="ui_select01" id="depotId" name="depotId">
								<option value="">全部仓库</option>
								<c:forEach var="ele" items="${depots}">
									<option value="${ele.id}">${ele.name}</option>
								</c:forEach>
							</select>
							品牌
							<select class="ui_select01" id="brandId" name="brandId">
								<option value="">全部品牌</option>
								<c:forEach var="ele" items="${brands}">
									<option value="${ele.id}">${ele.name}</option>
								</c:forEach>
							</select>
							阈值
							<input type="number" class="ui_input_txt01" name="limitNum" value="${qo.limitNum}"/>
							<script>
                                //回显仓库
                                $("#depotId option[value='${qo.depotId}']").prop("selected",true);
                                //回显品牌
                                $("#brandId option[value='${qo.brandId}']").prop("selected",true);
							</script>
						</div>
						<div id="box_bottom">
							<input type="button" value="查询" class="ui_input_btn01 btn_page"/>
						</div>
					</div>
				</div>
			</div>
			<div class="ui_content">
				<div class="ui_tb">
					<table class="table" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
						<tr>
							<th width="30"><input type="checkbox" id="all" /></th>
							<th>仓库</th>
							<th>货品编码</th>
							<th>货品名称</th>
							<th>货品品牌</th>
							<th>库存价格</th>
							<th>库存数量</th>
							<th>总价值</th>
						</tr>
						<c:forEach items="${result.data}" var="entity" >
							<tr>
								<td><input type="checkbox" class="acb" /></td>
								<td>${entity.d_name}</td>
								<td>${entity.p_sn}</td>
								<td>${entity.p_name}</td>
								<td>${entity.p_brandName}</td>
								<td>${entity.price}</td>
								<td>${entity.storeNumber}</td>
								<td>${entity.amount}</td>
							</tr>
						</c:forEach>
					</table>
					<%--引入分页操作--%>
					<%@include file="/WEB-INF/views/common/page.jsp"%>
				</div>
			</div>
		</div>
	</form>
</body>
</html>
