<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>信息管理系统</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
<link href="/style/common_style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/js/plugins/artDialog/jquery.artDialog.js?skin=blue"></script>
<script type="text/javascript" src="/js/plugins/artDialog/jquery.artDialog.js"></script>
<script type="text/javascript" src="/js/plugins/artDialog/iframeTools.js"></script>
<script type="text/javascript" src="/js/plugins/jqueryFrom/jQueryForm.js"></script>
<script src="/js/plugins/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/js/commonAll.js"></script>
	<script>
		$(function () {
			$("#editForm").ajaxForm(function (data) {
				showDialog("保存成功",function () {
					location.href="/stockIncomeBill/list.do";
                });
            });
			//保存按钮事件
			$(".btn_submit").click(function () {
				//遍历出4个提交到后台的数据
				$.each($("#edit_table_body tr"),function (index,tr) {
					//获取到4个提交的数据,修改其索引名称
					$(tr).find("[tag='pid']").prop("name","items["+index+"].product.id");
					$(tr).find("[tag='costPrice']").prop("name","items["+index+"].costPrice");
					$(tr).find("[tag='number']").prop("name","items["+index+"].number");
					$(tr).find("[tag='remark']").prop("name","items["+index+"].remark");
                });
				//提交表单
				$("#editForm").submit();
            });
			//日历插件
			$(".Wdate").click(function () {
				WdatePicker({readOnly:true});
            });
			//统一事件管理
			$("#edit_table_body").on("click",".searchproduct",function () {
                //放大镜单机事件
				//获取当前行
				var tr=$(this).closest("tr");
                //子窗口
                $.dialog.open("/product/productListViews.do", {
                    title: "选中商品",
                    width: "90%",
                    height: "90%",
                    left: "50%",
                    top: "50%",
                    lock: true,
                    resize: false,
                    close: function () {
                        //获取子窗口的共享数据,然后清空
                        var data=$.dialog.data("data");
                        if(data){
                            $.dialog.removeData("data");
                            //回填数据
							tr.find("[tag='name']").val(data.name);
							tr.find("[tag='pid']").val(data.id);
							tr.find("[tag='brand']").html(data.brandName);
							tr.find("[tag='costPrice']").val(data.costPrice);
							tr.find("[tag='number']").val(1);//设置数量默认值为1
							tr.find("[tag='amount']").html(data.costPrice.toFixed(2));//设置小数点后2位
                        }
                    }
                },false);
            }).on("click",".removeItem",function () {
				//删除一行明细
				//获取当前行
				var tr=$(this).closest("tr");
				//当只剩下最后一行时不进行删除,进行清空操作
				if($("#edit_table_body tr").size()==1){
				    tr.find("input").val("");
				    tr.find("span").html("");
				    return;
				}
				//如果不是最后一行则采用删除操作
				tr.remove();
            });
			//对单价,数量绑定失去焦点事件
			$("input[tag='costPrice'],input[tag='number']").blur(function () {
				//获取到当前行
				var tr=$(this).closest("tr");
				//在当前行获取到单价和数量
				var costPrice=tr.find("[tag='costPrice']").val()||0;
				var number=tr.find("[tag='number']").val()||0;
				var amount=costPrice*number;
				tr.find("[tag='amount']").html(amount.toFixed(2))
            });
			$(".appendRow").click(function () {
                //添加明细
                //克隆第一行数据
                var tr=$("#edit_table_body tr:first").clone(true);
                //将克隆体中的数据进行清空操作
                tr.find("input").val("");
                tr.find("span").html("");
                //将克隆体加入到其中
                tr.appendTo("#edit_table_body");
            });
        });
	</script>
</head>
<body>
<form id="editForm" action="/stockIncomeBill/saveOrUpdate.do" method="post">
	<input type="hidden" name="id" value="${entity.id}">
	<div id="container">
		<div id="nav_links">
			<span style="color: #1A5CC6;">采购订单编辑</span>
			<div id="page_close">
				<a>
					<img src="/images/common/page_close.png" width="20" height="20" style="vertical-align: text-top;"/>
				</a>
			</div>
		</div>
		<div class="ui_content">
			<table cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
				<tr>
					<td class="ui_text_rt" width="140">采购订单号</td>
					<td class="ui_text_lt">
						<input name="sn" value="${entity.sn}" class="ui_input_txt02"/>
					</td>
				</tr>
				<tr>
					<td class="ui_text_rt" width="140">仓库</td>
					<td class="ui_text_lt">
						<select class="ui_select01" id="depotId" name="depot.id">
							<c:forEach var="ele" items="${depots}">
								<option value="${ele.id}">${ele.name}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<script>
					$("#depotId option[value='${entity.depot.id}']").prop("selected",true);
				</script>
				<tr>
					<td class="ui_text_rt" width="140">业务时间</td>
					<td class="ui_text_lt">
						<fmt:formatDate var="vdate" value="${entity.vdate}"/>
						<input name="vdate" value="${vdate}"
							   class="ui_input_txt02 Wdate"/>
					</td>
				</tr>
				<tr>
					<td class="ui_text_rt" width="140">明细</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="button" value="添加明细" class="ui_input_btn01 appendRow"/>
						<table class="edit_table" cellspacing="0" cellpadding="0" border="0">
							<thead>
							<tr>
								<th width="10"></th>
								<th width="170">货品</th>
								<th width="100">品牌</th>
								<th width="80">价格</th>
								<th width="80">数量</th>
								<th width="100">金额小计</th>
								<th width="180">备注</th>
								<th width="120"></th>
							</tr>
							</thead>
							<tbody id="edit_table_body">
							<c:choose>
								<%--新增用静态--%>
								<c:when test="${empty entity.id}">
									<tr>
										<td></td>
										<td>
											<input disabled readonly class="ui_input_txt01" tag="name"/>
											<img src="/images/common/search.png" class="searchproduct"/>
											<input type="hidden" name="items.product.id" tag="pid"/>
										</td>
										<td><span tag="brand"></span></td>
										<td><input type="number" tag="costPrice" name="items.costPrice" class="ui_input_txt01"/></td>
										<td><input type="number" tag="number" name="items.number" class="ui_input_txt01"/></td>
										<td><span tag="amount"></span></td>
										<td><input tag="remark" name="items.remark" class="ui_input_txt01"/></td>
										<td>
											<a href="javascript:;" class="removeItem">删除明细</a>
										</td>
									</tr>
								</c:when>
								<%--更新用动态--%>

								<c:otherwise>
								<c:forEach items="${entity.items}" var="item">
									<tr>
										<td></td>
										<td>
											<input disabled readonly class="ui_input_txt01" tag="name" value="${item.product.name}"/>
											<img src="/images/common/search.png" class="searchproduct"/>
											<input type="hidden" name="items.product.id" tag="pid" value="${item.product.id}"/>
										</td>
										<td><span tag="brand">${item.product.brandName}</span></td>
										<td><input type="number" tag="costPrice" name="items.costPrice" class="ui_input_txt01" value="${item.costPrice}"/></td>
										<td><input type="number" tag="number" name="items.number" class="ui_input_txt01" value="${item.number}"/></td>
										<td><span tag="amount">${item.amount}</span></td>
										<td><input tag="remark" name="items.remark" class="ui_input_txt01" value="${item.remark}"/></td>
										<td>
											<a href="javascript:;" class="removeItem">删除明细</a>
										</td>
									</tr>
								</c:forEach>
								</c:otherwise>
							</c:choose>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="ui_text_lt">
						&nbsp;<input type="button" value="确定保存" class="ui_input_btn01 btn_submit" />
						&nbsp;<input id="cancelbutton" type="button" value="重置" class="ui_input_btn01"/>
					</td>
				</tr>
				<script>

				</script>
			</table>
		</div>
	</div>
</form>
</body>
</html>