<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
<link href="/style/common_style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/js/plugins/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/js/plugins/artDialog/jquery.artDialog.js?skin=blue"></script>
<script type="text/javascript" src="/js/commonAll.js"></script>
<script>
	$(function () {
		$(".btn_audit").click(function () {
		    var url=$(this).data("url");
			showDialog("确定审核?",function () {
                $.get(url,function (data) {
                    if(data.success){
                        showDialog("审核成功",function () {
                        location.reload();
                        });
                    }else{
                     	showDialog(data.msg);
					}
                },"json");
            },true);
        });
        //日历插件
        //处理开始日期
		$("input[name='beginDate']").click(function () {
            WdatePicker({
				readOnly:true,
				maxDate:new Date()
            });
        });
        //处理结束日期
        $("input[name='endDate']").click(function () {
            WdatePicker({
                readOnly:true,
				minDate:$("input[name='beginDate']").val(),
                maxDate:new Date()
            });
        });
    });
</script>
<title>WMS-出库订单管理</title>
<style>
	.alt td{ background:black !important;}
</style>
</head>
<body>
	<form id="searchForm" action="/stockOutcomeBill/list.do" method="post">
		<div id="container">
			<div class="ui_content">
				<div class="ui_text_indent">
					<div id="box_border">
						<div id="box_top">搜索</div>
						<div id="box_center">
							业务时间:
							<fmt:formatDate value="${qo.beginDate}" var="beginDate"/>
							<fmt:formatDate value="${qo.endDate}" var="endDate"/>
							<input type="text" class="ui_input_txt02 Wdate" name="beginDate" value="${beginDate}"/>
							~<input type="text" class="ui_input_txt02 Wdate" name="endDate" value="${endDate}"/>
							仓库:
							<select class="ui_select01" id="depotId" name="depotId">
								<option value="-1">全部仓库</option>
								<c:forEach var="ele" items="${depots}">
									<option value="${ele.id}">${ele.name}</option>
								</c:forEach>
							</select>
							客户:
							<select class="ui_select01" id="clientId" name="clientId">
								<option value="-1">全部客户</option>
								<c:forEach var="ele" items="${clients}">
									<option value="${ele.id}">${ele.name}</option>
								</c:forEach>
							</select>
							状态:
							<select class="ui_select01" id="status" name="status">
								<option value="-1">全部</option>
								<option value="1">未审核</option>
								<option value="2">已审核</option>
							</select>
							<script>
                                //回显仓库
                                $("#depotId option[value='${qo.depotId}']").prop("selected",true);
                                //回显客户
                                $("#clientId option[value='${qo.clientId}']").prop("selected",true);
                                //回显状态
								$("#status option[value='${qo.status}']").prop("selected",true);
							</script>
						</div>
						<div id="box_bottom">
							<input type="button" value="查询" class="ui_input_btn01 btn_page"
								   data-page="1"/>
							<input type="button" value="新增" class="ui_input_btn01 btn_input"
									data-url="/stockOutcomeBill/input.do"/>
						</div>
					</div>
				</div>
			</div>
			<div class="ui_content">
				<div class="ui_tb">
					<table class="table" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
						<tr>
							<th width="30"><input type="checkbox" id="all" /></th>
							<th>出库订单号</th>
							<th>业务时间</th>
							<th>仓库</th>
							<th>客户</th>
							<th>总数</th>
							<th>总金额</th>
							<th>录入人</th>
							<th>审核人</th>
							<th>状态</th>
							<th></th>
						</tr>
						<c:forEach items="${result.data}" var="entity" varStatus="num">
							<tr>
								<td><input type="checkbox" class="acb" /></td>
								<td>${entity.sn}</td>
								<td><fmt:formatDate value="${entity.vdate}"/></td>
								<td>${entity.depot.name}</td>
								<td>${entity.client.name}</td>
								<td>${entity.totalNumber}</td>
								<td>${entity.totalAmount}</td>
								<td>${entity.inputUser.name}</td>
								<td>${entity.auditor.name}</td>
								<td>
									<c:choose>
									<c:when test="${entity.status==1}">
										<span style="color: green">未审核</span>
									</c:when>
									<c:when test="${entity.status==2}">
										<span style="color: red">已审核</span>
									</c:when>
								</c:choose></td>
								<td>

									<c:choose>
										<c:when test="${entity.status==1}">
									<a href="/stockOutcomeBill/input.do?id=${entity.id}">编辑</a>
									<a href="javascript:;" class="btn_delete"
									   data-url="/stockOutcomeBill/delete.do?id=${entity.id}">删除</a>
									<a href="javascript:;" class="btn_audit"
											data-url="/stockOutcomeBill/audit.do?id=${entity.id}">审核</a>
										</c:when>
									<c:when test="${entity.status==2}">
									<a href="/stockOutcomeBill/view.do?id=${entity.id}">查看</a>
									</c:when>
									</c:choose>
								</td>
							</tr>
						</c:forEach>

					</table>
				</div>
				<%--引入分页操作--%>
				<%@include file="/WEB-INF/views/common/page.jsp"%>
			</div>
		</div>
	</form>
</body>
</html>
