<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>信息管理系统</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
<link href="/style/common_style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/js/plugins/artDialog/jquery.artDialog.js?skin=blue"></script>
<script type="text/javascript" src="/js/plugins/jqueryFrom/jQueryForm.js"></script>
<script type="text/javascript" src="/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="/js/validation/messages_cn.js"></script>
<script type="text/javascript" src="/js/commonAll.js"></script>

<script>
	//表单校验
	$(function () {
		$("#editForm").validate({
			rules:{
			    name:{
			       required:"true",
					rangelength:[2,6]
				},
				password:{
                    required:"true",
					number:"true"

				},
				email:{
                    required:"true",
                    email:"true"
				},
				age:{
                    required:"true",
                    number:"true",
                    range:[16,60]
				}
			}
		});

		//是否是超级管理员
		var cp=$(".role").clone(true);//拷贝角色分配
		$(":checkbox[name='admin']").click(function () {
			if(this.checked){
			    $(".role").remove();
			}else{
			    $(this).closest("tr").after(cp);
			}
        });
        //判断复选框默认状态是否是超级管理员
        if($(":checkbox[name='admin']").prop("checked")){
            $(".role").remove();
        }
		//弹框
        $("#editForm").ajaxForm(function (data) {
            showDialog("保存成功",function () {
                location.href="/employee/list.do";
            });
        });
        //表单提交选中所有
        $(".btn_submit").click(function () {
            //选中右边所有的option
            $(".selected_permission option").prop("selected",true);
            //提交表单
            $("#editForm").submit();
        });
    });
</script>
</head>
<body>
<form id="editForm" action="/employee/saveOrUpdate.do" method="post">
	<input type="hidden" name="id" value="${entity.id}">
	<div id="container">
		<div id="nav_links">
			<span style="color: #1A5CC6;">用户编辑</span>
			<div id="page_close">
				<a>
					<img src="/images/common/page_close.png" width="20" height="20" style="vertical-align: text-top;"/>
				</a>
			</div>
		</div>
		<div class="ui_content">
			<table cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
				<tr>
					<td class="ui_text_rt" width="140">用户名</td>
					<td class="ui_text_lt">
						<input name="name" class="ui_input_txt02" value="${entity.name}"/>
					</td>
				</tr>
				<c:if test="${empty entity.id}">
				<tr>
					<td class="ui_text_rt" width="140">密码</td>
					<td class="ui_text_lt">
						<input type="password" name="password" id="password" class="ui_input_txt02"/>
					</td>
				</tr>
				<tr>
					<td class="ui_text_rt" width="140">验证密码</td>
					<td class="ui_text_lt">
						<input name="repassword" type="password" class="ui_input_txt02" />
					</td>
				</tr>
				</c:if>
				<tr>
					<td class="ui_text_rt" width="140">Email</td>
					<td class="ui_text_lt">
						<input name="email" class="ui_input_txt02" value="${entity.email}"/>
					</td>
				</tr>
				<tr>
					<td class="ui_text_rt" width="140">年龄</td>
					<td class="ui_text_lt">
						<input name="age" class="ui_input_txt02" value="${entity.age}"/>
					</td>
				</tr>
				<tr>
					<td class="ui_text_rt" width="140">所属部门</td>
					<td class="ui_text_lt">
						<select name="dept.id" class="ui_select03">
							<c:forEach items="${depts}" var="ele">
							<option value="${ele.id}">${ele.name}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td class="ui_text_rt" width="140" >超级管理员</td>
					<td class="ui_text_lt">
						<input type="checkbox" name="admin" id="admin" class="ui_checkbox01" />
					</td>

				</tr>
				<script>
				<%--更新操作时判断是否是超级管理员--%>
				<c:if test="${not empty entity.id}">
					$("#admin").prop("checked",${entity.admin});
				</c:if>
				</script>
				<tr class="role">
					<td class="ui_text_rt" width="140">分配角色</td>
					<td class="ui_text_lt">
						<table>
							<tr>
								<td>
									<select multiple="true" class="ui_multiselect01 all_permission">
										<c:forEach items="${role}" var="ele">
											<option value="${ele.id}">${ele.name}</option>
										</c:forEach>
									</select>
								</td>
								<td align="center">
									<input type="button" id="select" value="-->" class="left2right"/><br/>
									<input type="button" id="selectAll" value="==>" class="left2right"/><br/>
									<input type="button" id="deselect" value="<--" class="left2right"/><br/>
									<input type="button" id="deselectAll" value="<==" class="left2right"/>
								</td>
								<td>
									<select name="ids" multiple="true" class="ui_multiselect01 selected_permission">
										<c:forEach items="${entity.roles}" var="ele">
											<option value="${ele.id}">${ele.name}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="ui_text_lt">
						&nbsp;<input type="button" value="确定保存" class="ui_input_btn01 btn_submit"/>
						&nbsp;<input id="cancelbutton" type="button" value="重置" class="ui_input_btn01"/>
					</td>
				</tr>
			</table>
		</div>
	</div>
</form>
</body>
</html>