
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script type="text/javascript" src="/js/plugins/echarts/echarts.common.min.js"></script>
</head>
<body>
<div id="main" style="width:600px;height:600px;">
    <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById("main"));
        // 指定图表的配置项和数据
        var option = {
            title : {
                text: "销售报表",
                subtext: "${groupType}",
                x:"center"
            },
            tooltip : {
                trigger: "item",
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: "vertical",
                left: "left",
                data: ${x}
            },
            series : [
                {
                    name: "访问来源",
                    type: "pie",
                    radius : "55%",
                    center: ["60%", "60%"],
                    data:${datas},
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: "rgba(0, 0, 0, 0.5)"
                        }
                    }
                }
            ]
        };
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    </script>
</div>
</body>
</html>
