<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
<link href="/style/common_style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/js/plugins/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/js/plugins/artDialog/jquery.artDialog.js?skin=blue"></script>
<script type="text/javascript" src="/js/commonAll.js"></script>
<script>
	$(function () {
        //日历插件
        //处理开始日期
		$("input[name='beginDate']").click(function () {
            WdatePicker({
				readOnly:true,
				maxDate:new Date()
            });
        });
        //处理结束日期
        $("input[name='endDate']").click(function () {
            WdatePicker({
                readOnly:true,
				minDate:$("input[name='beginDate']").val(),
                maxDate:new Date()
            });
        });
        //跳转事件
        $(".btn_page").click(function () {
            var page=$(this).data("page")||$(":text[name='currentPage']").val();
            $(":text[name='currentPage']").val(page);
            $("#searchForm").submit();
        });
    });
</script>
<title>订货报表</title>
<style>
	.alt td{ background:black !important;}
</style>
</head>
<body>
	<form id="searchForm" action="/chart/order.do" method="post">
		<div id="container">
			<div class="ui_content">
				<div class="ui_text_indent">
					<div id="box_border">
						<div id="box_top">搜索</div>
						<div id="box_center">
							业务时间:
							<fmt:formatDate value="${qo.beginDate}" var="beginDate"/>
							<fmt:formatDate value="${qo.endDate}" var="endDate"/>
							<input type="text" class="ui_input_txt01 Wdate" name="beginDate" value="${beginDate}"/>
							~<input type="text" class="ui_input_txt01 Wdate" name="endDate" value="${endDate}"/>
							商品名称/编码
							<input type="text" class="ui_input_txt01" name="keyword" value="${qo.keyword}"/>
							供应商:
							<select class="ui_select01" id="supplierId" name="supplierId">
								<option value="-1">全部供应商</option>
								<c:forEach var="ele" items="${suppliers}">
									<option value="${ele.id}">${ele.name}</option>
								</c:forEach>
							</select>
							品牌:
							<select class="ui_select01" id="brandId" name="brandId">
								<option value="-1">全部</option>
								<c:forEach var="ele" items="${brands}">
									<option value="${ele.id}">${ele.name}</option>
								</c:forEach>
							</select>
							类型:
							<select class="ui_select01" id="groupType" name="groupType">
								<option value="e.name">订货员</option>
								<option value="p.name">商品名称</option>
								<option value="p.brandName">商品品牌</option>
								<option value="s.name">供应商</option>
								<option value="DATE_FORMAT(bill.vdate,'%Y-%m')">订货月份</option>
								<option value="DATE_FORMAT(bill.vdate,'%Y-%m-%d')">订货日期</option>
							</select>
							<script>
                                //回显供应商
                                $("#supplierId option[value='${qo.supplierId}']").prop("selected",true);
                                //回显品牌
								$("#brandId option[value='${qo.brandId}']").prop("selected",true);
								//回显类型
								$("#groupType option[value=\"${qo.groupType}\"]").prop("selected",true);
							</script>
						</div>
						<div id="box_bottom">
							<input type="button" value="查询" class="ui_input_btn01 btn_page"
								   data-page="1"/>
						</div>
					</div>
				</div>
			</div>
			<div class="ui_content">
				<div class="ui_tb">
					<table class="table" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
						<tr>
							<th>分组类型</th>
							<th>订单总数</th>
							<th>订单总额</th>
						</tr>
						<c:forEach items="${list}" var="entity">
							<tr>
								<td>${entity.groupType}</td>
								<td>${entity.totaNumber}</td>
								<td>${entity.totaAmount}</td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
		</div>
	</form>
</body>
</html>
