//禁用数组参数添加 [ ] 的功能
$.ajaxSettings.traditional=true;
/** table鼠标悬停换色* */
$(function () {
    // 如果鼠标移到行上时，执行函数
    $(".table tr").mouseover(function () {
        $(this).css({
            background: "#CDDAEB"
        });
        $(this).children('td').each(function (index, ele) {
            $(ele).css({
                color: "#1D1E21"
            });
        });
    }).mouseout(function () {
        $(this).css({
            background: "#FFF"
        });
        $(this).children('td').each(function (index, ele) {
            $(ele).css({
                color: "#909090"
            });
        });
    });
});

//编辑页面跳转
$(function () {
    $(".btn_input").click(function () {
        //获取自定义属性
        var url=$(this).data("url");
        //跳转
        location.href=url;
    });
});
//文档加载完毕执行函数
$(function () {
    //删除事件
    $(".btn_delete").click(function () {
        //获取自定义属性值
        var url =$(this).data("url");
        //弹框
        showDialog("是否要删除",function () {
           //发送删除请求
           $.get(url,function (data) {
               showDialog("删除成功",function () {
                   //刷新页面
                   location.reload();
               });
           },"json");
       },true);

    });
//---------------------
    //批量删除复选框
    $("#all").click(function () {
        $(".acb").prop("checked",this.checked);
    });
    //批量删除事件
    $(".btn_batchDelete").click(function () {
        //获取到自定义属性data-url
        var url=$(this).data("url");
        //使用弹框提示是否要删除
        showDialog("确定使用批量删除吗?会丢失所有数据的哦",function () {
            //获取选中的复选框并判断是否有被选中的复选框
            if (!$(".acb:checked").size()) {
                //如果没有选中的复选框则提示"至少需要勾选一个数据"
                showDialog("至少需要勾选一个数据");
                //没有数据则结束不执行下面的操作
                return;
            }
            //获取到复选框中的被选中的数据的id存放进map集合中
            var ids=$.map($(".acb:checked"),function (ele) {
                return $(ele).data("eid");
            });
            //发送ajax请求,执行批量删除
            $.post(url,{ids:ids},function (data) {
                showDialog("你成功的删除了所有的数据",function () {
                    //刷新页面
                    location.reload();
                },true);
            });
        });
    });
});
//显示对话框
function showDialog(content,ok,cancel) {
    $.dialog({
       title:"温馨提示",
        content:content, //消息
        ok:ok||true,    //确定按钮
        cancel:cancel,  //取消按钮
        icon:"face-smile" //图标
    });
}
//文档加载完毕执行函数
$(function () {
    //左移右移
    //选中右移
    $("#select").click(function () {
        $(".all_permission option:selected").appendTo(".selected_permission");
    });
    //全部右移
    $("#selectAll").click(function () {
        $(".all_permission option").appendTo(".selected_permission");
    });
    //选中左移
    $("#deselect").click(function () {
        $(".selected_permission option:selected").appendTo(".all_permission");
    });
    //全部左移
    $("#deselectAll").click(function () {
        $(".selected_permission option").appendTo(".all_permission");
    });

    //消除重复
    var arr=$.map($(".selected_permission option"),function (ele) {
        //获取到option的value值
        return ele.value;
    });
    //遍历数组获取元素的value值
    $.each(arr,function (index,val) {
        //遍历左边的option
        $.each($(".all_permission option"),function (i,ele) {
            //判断是否重复
            if(val==ele.value){
                //重复删除
                $(ele).remove();
            }
        });
    });
});