package cn.wolfcode.wms.util;

import cn.wolfcode.wms.domain.Employee;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
@Retention(RetentionPolicy.RUNTIME)//设置能加载到JVM中
@Target(ElementType.METHOD)//设置能贴在方法上
public @interface RequiredPermission {
    String value();
}
