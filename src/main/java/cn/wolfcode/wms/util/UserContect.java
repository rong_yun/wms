package cn.wolfcode.wms.util;

import cn.wolfcode.wms.domain.Employee;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;

public abstract class UserContect {
    public  static void setCurrentUser(Employee e){
        HttpSession session = ((ServletRequestAttributes) (RequestContextHolder.getRequestAttributes())).getRequest().getSession();
        session.setAttribute("EM_IN_SESSION",e);
    }
    public  static Employee getCurrentUser(){
        HttpSession session = ((ServletRequestAttributes) (RequestContextHolder.getRequestAttributes())).getRequest().getSession();
        return (Employee) session.getAttribute("EM_IN_SESSION");
    }

    public  static void setExpression(List<String> exps){
        HttpSession session = ((ServletRequestAttributes) (RequestContextHolder.getRequestAttributes())).getRequest().getSession();
        session.setAttribute("EXP_IN_SESSION",exps);
    }
    public  static List<String> getExpression(){
        HttpSession session = ((ServletRequestAttributes) (RequestContextHolder.getRequestAttributes())).getRequest().getSession();
        return (List<String>) session.getAttribute("EXP_IN_SESSION");
    }
}
