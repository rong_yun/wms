package cn.wolfcode.wms.util;

import cn.wolfcode.wms.domain.Employee;
import lombok.Getter;

import java.util.Collections;
import java.util.List;
@Getter
public class PageResult {
    //空对象
    public static final PageResult
            EMPTY_PAGE =new PageResult(1,1,0, Collections.EMPTY_LIST);
    private int currentPage;//当前页
    private int pageSize;//页面容量
    private Integer rows;//总记录数
    private List<?> data;//记过集
    private int endPage;//末页
    private int prevPage;//上一页
    private int nextPage;//下一页
    public PageResult(int currentPage, int pageSize, Integer rows, List<?> data) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.rows = rows;
        this.data = data;
        if (rows<pageSize){
            endPage=1;
            nextPage=1;
            prevPage=1;
            return;
        }
        endPage=rows%pageSize != 0 ? rows/pageSize+1:rows/pageSize;
        nextPage=currentPage<endPage?currentPage+1:endPage;
        prevPage=currentPage>1?currentPage-1:1;
    }
}
