package cn.wolfcode.wms.domain;

import lombok.*;

//品牌
@Setter@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Brand extends BaseDomain{
    private String name;
    private String sn;
}
