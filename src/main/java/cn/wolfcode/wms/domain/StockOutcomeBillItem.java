package cn.wolfcode.wms.domain;

import lombok.*;

import java.math.BigDecimal;

@Setter@Getter
@AllArgsConstructor@NoArgsConstructor
@ToString
public class StockOutcomeBillItem extends BaseDomain{
    private BigDecimal salePrice;//销售价
    private BigDecimal number;//销售数量
    private BigDecimal amount;//明细小计
    private String remark;//备注

    private Product product;//出库商品
    private Long billId;//出库单ID

}
