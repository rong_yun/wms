package cn.wolfcode.wms.domain;

import lombok.*;

@Setter@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SystemMenu extends BaseDomain{
    private String name;//菜单名称
    private String url;//url
    private String sn;//编码

    //父级菜单
    private SystemMenu parent;
}
