package cn.wolfcode.wms.domain;

import lombok.*;

//权限
@Setter@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Permission extends BaseDomain{
    private String name;
    private String expression;
}
