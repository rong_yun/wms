package cn.wolfcode.wms.domain;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

//角色
@Setter@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Role extends BaseDomain{
    private String name;//名称
    private String sn;//编码
    //关联关系many2many
    private List<Permission> permissions=new ArrayList<>();
    //关联关系
    private List<Permission> menus=new ArrayList<>();
}
