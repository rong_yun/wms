package cn.wolfcode.wms.domain;

import cn.wolfcode.wms.util.JSONResult;
import cn.wolfcode.wms.util.JSONUtil;
import lombok.*;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

//商品管理
@Setter@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Product extends BaseDomain{
    private String name;    //商品名称
    private String sn;      //商品编号
    private BigDecimal costPrice;   //成本价格
    private BigDecimal salePrice;   //零售价格
    private String imagePath;   //图片在服务器中的路径
    private String intro;   //商品简介

    private Long brandId;   //品牌ID
    private String brandName;   //品牌名称

    public String getSmallImage(){
        if(StringUtils.hasLength(imagePath)) {
            int end=imagePath.lastIndexOf('.');
            String filename = imagePath.substring(0, end);
            String ext = imagePath.substring(end);
            return filename+"_small"+ext;
        }
        return null;
    }
    //把当前对象的部分信息转成json
    public String getJsonString(){
        Map<String ,Object> map=new HashMap<>();
        map.put("id",id);
        map.put("name",name);
        map.put("costPrice",costPrice);
        map.put("salePrice",salePrice);
        map.put("brandName",brandName);
        return JSONUtil.toJSONString(map);
    }
}
