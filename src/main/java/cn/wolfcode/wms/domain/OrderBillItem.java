package cn.wolfcode.wms.domain;

import lombok.*;

import java.math.BigDecimal;
@Setter@Getter
@AllArgsConstructor@NoArgsConstructor
@ToString
public class OrderBillItem extends BaseDomain{
    private BigDecimal costPrice;//成本价
    private BigDecimal number;//采购数量
    private BigDecimal amount;//明细小计
    private String remark;//备注

    private Product product;//采购商品
    private Long billId;//采购单ID

}
