package cn.wolfcode.wms.domain;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

//销售账单
@Setter@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SaleAcCount extends BaseDomain{
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date vdate;//记账时间
    private BigDecimal number;//销售数量
    private BigDecimal costPrice;//成本价
    private BigDecimal costAmount;//总成本
    private BigDecimal salePrice;//销售价
    private BigDecimal saleAmount;//总销售额

    private Product product;//销售商品
    private Employee saleMan;//销售员
    private Client client;//客户
}
