package cn.wolfcode.wms.domain;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//品牌
@Setter@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StockIncomeBill extends BaseDomain{
    public static final Integer NORMAL=1;//未审核
    public static final Integer AUDIT=2;//已审核

    private String sn;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date vdate;//业务时间
    private Integer status=NORMAL;
    private BigDecimal totalAmount;
    private BigDecimal totalNumber;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date auditTime;//审核时间
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date inputTime;//录入时间

    private Employee inputUser;//录入人
    private Employee auditor;//审核人
    private Depot depot;//仓库

    //组合关系,包含采购明细
    private List<StockIncomeBillItem> items=new ArrayList<>();
}
