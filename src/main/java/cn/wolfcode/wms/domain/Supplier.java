package cn.wolfcode.wms.domain;

import lombok.*;

//商品
@Setter@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Supplier extends BaseDomain{
    private String name;
    private String phone;
    private String address;
}
