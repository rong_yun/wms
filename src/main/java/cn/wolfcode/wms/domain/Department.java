package cn.wolfcode.wms.domain;

import lombok.*;

//部门
@Setter@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Department extends BaseDomain{
    private String name;
    private String sn;
}
