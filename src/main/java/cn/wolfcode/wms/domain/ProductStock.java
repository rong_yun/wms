package cn.wolfcode.wms.domain;

import lombok.*;

import java.math.BigDecimal;

@Setter@Getter
@AllArgsConstructor@NoArgsConstructor
@ToString
public class ProductStock extends BaseDomain{
    private BigDecimal price;//库存价格
    private BigDecimal storeNumber;//存储数量
    private BigDecimal amount;//明细小计

    private Product product;//采购商品
    private Depot depot;//仓库

}
