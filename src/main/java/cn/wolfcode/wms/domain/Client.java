package cn.wolfcode.wms.domain;

import lombok.*;

//客户
@Setter@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Client extends BaseDomain{
    private String name;//客户名称
    private String sn;//客户编码
    private String phone;//客户电话
 }
