package cn.wolfcode.wms.domain;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

//员工
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class   Employee extends BaseDomain{
    //姓名
    private String name;
    //密码
    private String password;
    //邮箱
    private String email;
    //年龄
    private Integer age;
    //是否是超级管理员
    private boolean admin;

    //关联关系many2one
    private Department dept;

    //关联关系many2many 员工关联角色
    private List<Role> roles=new ArrayList<>();

}