package cn.wolfcode.wms.mapper;

import cn.wolfcode.wms.domain.OrderBill;
import cn.wolfcode.wms.query.QueryObject;

import java.util.List;

public interface OrderBillMapper {
    int deleteByPrimaryKey(Long id);

    int insert(OrderBill record);

    OrderBill selectByPrimaryKey(Long id);

    int updateByPrimaryKey(OrderBill record);
    Integer queryForCount(QueryObject qo);

    List<?> queryForList(QueryObject qo);
    void deleteItemByBillId(Long billId);
    void audit(OrderBill record);
}