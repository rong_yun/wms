package cn.wolfcode.wms.mapper;

import cn.wolfcode.wms.domain.StockIncomeBillItem;

public interface StockIncomeBillItemMapper {
    int insert(StockIncomeBillItem record);

}