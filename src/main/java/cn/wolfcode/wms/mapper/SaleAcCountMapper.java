package cn.wolfcode.wms.mapper;

import cn.wolfcode.wms.domain.SaleAcCount;
import java.util.List;

public interface SaleAcCountMapper {
    int insert(SaleAcCount record);
}