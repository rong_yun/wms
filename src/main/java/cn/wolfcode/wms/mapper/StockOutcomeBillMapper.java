package cn.wolfcode.wms.mapper;

import cn.wolfcode.wms.domain.StockOutcomeBill;
import cn.wolfcode.wms.query.QueryObject;

import java.util.List;

public interface StockOutcomeBillMapper {
    int deleteByPrimaryKey(Long id);

    int insert(StockOutcomeBill record);

    StockOutcomeBill selectByPrimaryKey(Long id);

    int updateByPrimaryKey(StockOutcomeBill record);
    Integer queryForCount(QueryObject qo);

    List<?> queryForList(QueryObject qo);
    void deleteItemByBillId(Long billId);
    //单据审核
    void audit(StockOutcomeBill record);
}