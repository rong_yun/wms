package cn.wolfcode.wms.mapper;

import cn.wolfcode.wms.query.OrderChartQueryObject;
import cn.wolfcode.wms.query.SaleChartQueryObject;

import java.util.List;
import java.util.Map;

public interface ChartMapper {
   //使用map封装数据,再讲map存入list集合中
   List<Map<String,Object>> queryOrderChart(OrderChartQueryObject qo);
   List<Map<String,Object>> querySaleChart(SaleChartQueryObject qo);
}