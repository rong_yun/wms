package cn.wolfcode.wms.mapper;

import cn.wolfcode.wms.domain.Employee;
import cn.wolfcode.wms.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeMapper {
    int deleteByPrimaryKey(Long id);//删除
    int insert(Employee record);//添加
    Employee selectByPrimaryKey(Long id);//查询单个
    int updateByPrimaryKey(Employee record);//更新
    Integer queryForCount(QueryObject qo);//查询记录数
    List<?> queryForList(QueryObject qo);//条件查询

    void deleteRelation(Long id);//删除员工和角色的关系(中间表)
    //添加关系
    void insertRelation(@Param("employeeId") Long employeeId,
                        @Param("roleId") Long roleId);
    //查询用户名和密码(用于登录校验)
    Employee selectByInfo(@Param("username") String username,
                          @Param("password") String password);
    //批量删除
    void batchDelete(List<Long> ids);
}