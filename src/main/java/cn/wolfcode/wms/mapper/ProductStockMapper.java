package cn.wolfcode.wms.mapper;

import cn.wolfcode.wms.domain.Depot;
import cn.wolfcode.wms.domain.Product;
import cn.wolfcode.wms.domain.ProductStock;
import cn.wolfcode.wms.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductStockMapper {

    int insert(ProductStock record);



    int updateByPrimaryKey(ProductStock record);
    ProductStock selectByProductAndDepot(@Param("productId") Long productId,
                                         @Param("depotId") Long depotId);
    Integer queryForCount(QueryObject qo);

    List<?> queryForList(QueryObject qo);
}