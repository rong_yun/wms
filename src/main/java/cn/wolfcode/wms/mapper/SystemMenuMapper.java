package cn.wolfcode.wms.mapper;

import cn.wolfcode.wms.domain.SystemMenu;
import cn.wolfcode.wms.query.SystemMenuQueryObject;

import java.util.List;
import java.util.Map;

public interface SystemMenuMapper {
    int deleteByPrimaryKey(Long id);
    int insert(SystemMenu record);
    SystemMenu selectByPrimaryKey(Long id);
    List<SystemMenu> selectAll();

    int updateByPrimaryKey(SystemMenu record);
    List<SystemMenu> query(SystemMenuQueryObject qo);
    void deleteChildMenu(Long parentId);//根据父菜单删除子菜单
    List<Map<String,Object>> selectMenuBySn(String menuSn);//根据父菜单的sn查询子菜单

}