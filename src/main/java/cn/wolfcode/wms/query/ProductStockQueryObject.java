package cn.wolfcode.wms.query;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter@Setter

public class ProductStockQueryObject extends QueryObject {
    public ProductStockQueryObject() {
        setPageSize(20);
    }

    private String keyword;//名称或编码
    private Long depotId=-1l;//仓库
    private Long brandId=-1L;//品牌id
    private BigDecimal limitNum;//阈值

    public String getKeyword(){
        return empty2null(keyword);
    }
}
