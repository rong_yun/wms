package cn.wolfcode.wms.query;

import cn.wolfcode.wms.util.DateUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
@Getter
@Setter
public class StockOutcomeBillQueryObject extends QueryObject {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private java.util.Date endDate;
    private Long depotId=-1L;
    private Integer status=-1;
    private Long clientId=-1L;

    public Date getEndDate(){
        return DateUtil.getEndDate(endDate);
    }
}
