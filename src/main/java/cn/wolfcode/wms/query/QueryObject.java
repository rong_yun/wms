package cn.wolfcode.wms.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
public class QueryObject {
    private int currentPage=1;//当前页
    private int pageSize=20;//页面容量
    //传递分页参数
    public int getStack(){
        return (currentPage-1)*pageSize;
    }
    //判断字符串是否为+空
    protected String empty2null(String s){
        return StringUtils.hasLength(s)?s:null;
    }
}
