package cn.wolfcode.wms.query;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductQueryObject extends QueryObject{

    private String keyword;//名称或编码

    public ProductQueryObject() {
        setPageSize(20);
    }

    private Long brandId=-1L;//品牌id

    public String getKeyword(){
        return empty2null(keyword);
    }
}
