package cn.wolfcode.wms.query;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeQueryObject extends QueryObject{
    private String keyword;//关键字
    private Long deptId=-1L;//部门id
    public String getKeyword(){ //判断是否为空值
        return empty2null(keyword);
    }
}
