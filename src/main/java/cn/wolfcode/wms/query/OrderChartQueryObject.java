package cn.wolfcode.wms.query;

import cn.wolfcode.wms.util.DateUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
@Getter@Setter
public class OrderChartQueryObject extends QueryObject{
    //业务时间区间
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginDate;//开始时间
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private  Date endDate;//结束时间
    private String keyword;//关键字查询
    private Long supplierId=-1L;//供应商ID
    private Long brandId=-1L;//品牌
    private String groupType="e.name";//分组类型

    public Date getEndDate(){
        return DateUtil.getEndDate(endDate);
    }
    //判断keyword是否为空值
    public String getKeyword(){
        return empty2null(keyword);}
}
