package cn.wolfcode.wms.query;

import cn.wolfcode.wms.util.DateUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
@Getter@Setter
public class SaleChartQueryObject extends QueryObject{
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private  Date endDate;
    private String keyword;
    private Long clientId=-1L;//客户ID
    private Long brandId=-1L;//品牌
    private String groupType="e.name";//分组类型

    public Date getEndDate(){
        return DateUtil.getEndDate(endDate);
    }
    public String getKeyword(){
        return empty2null(keyword);}
}
