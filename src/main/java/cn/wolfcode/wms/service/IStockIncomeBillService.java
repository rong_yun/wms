package cn.wolfcode.wms.service;

import cn.wolfcode.wms.domain.StockIncomeBill;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.util.PageResult;

public interface IStockIncomeBillService {
    void saveOrUpdate(StockIncomeBill entity);
    void delete(Long id);
    StockIncomeBill get(Long id);
    PageResult query(QueryObject qo);
    //审核
    void audit(Long id);
}
