package cn.wolfcode.wms.service;

import cn.wolfcode.wms.query.OrderChartQueryObject;
import cn.wolfcode.wms.query.SaleChartQueryObject;

import java.util.List;
import java.util.Map;

public interface IChartService {
    List<Map<String,Object>> queryOrderChart(OrderChartQueryObject qo);
    List<Map<String,Object>> querySaleChart(SaleChartQueryObject qo);
}
