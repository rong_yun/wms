package cn.wolfcode.wms.service;

import cn.wolfcode.wms.domain.SystemMenu;
import cn.wolfcode.wms.query.SystemMenuQueryObject;

import java.util.List;
import java.util.Map;

public interface ISystemMenuService {
    void saveOrUpdate(SystemMenu entity);
    void delete(Long id);
    SystemMenu get(Long id);
    List<SystemMenu> list();
    //根据父菜单做查询
    List<SystemMenu> query(SystemMenuQueryObject qo);

    List<SystemMenu> getParentMenus(Long menuId);

    List<Map<String,Object>> getMenusBySn(String menuSn);
}
