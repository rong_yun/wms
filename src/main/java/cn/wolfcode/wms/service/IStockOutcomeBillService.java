package cn.wolfcode.wms.service;

import cn.wolfcode.wms.domain.StockOutcomeBill;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.util.PageResult;

public interface IStockOutcomeBillService {
    void saveOrUpdate(StockOutcomeBill entity);
    void delete(Long id);
    StockOutcomeBill get(Long id);
    PageResult query(QueryObject qo);
    //审核
    void audit(Long id);
}
