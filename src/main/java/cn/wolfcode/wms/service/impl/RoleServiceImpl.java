package cn.wolfcode.wms.service.impl;

import cn.wolfcode.wms.domain.Role;
import cn.wolfcode.wms.mapper.RoleMapper;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IRoleService;
import cn.wolfcode.wms.util.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements IRoleService {
    @Autowired
    private RoleMapper roleMapper;
    //添加或更新
    public void saveOrUpdate(Role entity, Long[] menuIds, Long[] ids) {
        if(entity.getId()!=null){
            roleMapper.deleteRelation(entity.getId());//删除旧的权限
            roleMapper.deleteMenuRelation(entity.getId());//中间表
            roleMapper.updateByPrimaryKey(entity);
        }else {
            roleMapper.insert(entity);
        }
        //处理关联关系
        if(ids!=null){
            for (Long permissionId : ids) {
                roleMapper.insertRelation(entity.getId(),permissionId);
            }
        }  //处理关联关系
        if(ids!=null){
            for (Long menuId : menuIds) {
                roleMapper.insertMenuRelation(entity.getId(),menuId);
            }
        }
    }
    //删除
    public void delete(Long id) {
            //要删除角色必须先删除关联关系(即先删除中间表)
            roleMapper.deleteRelation(id);
            roleMapper.deleteByPrimaryKey(id);
            roleMapper.deleteMenuRelation(id);
    }
    //获取单个
    public Role get(Long id) {
        return roleMapper.selectByPrimaryKey(id);
    }
    //查询所有
    public List<Role> list() {
        return roleMapper.selectAll();
    }


    public PageResult query(QueryObject qo) {
        //查询总记录数
        Integer rows=roleMapper.queryForCount(qo);
        if(rows==0){
            return PageResult.EMPTY_PAGE;
        }
        //查询列表
        List<?> data=roleMapper.queryForList(qo);
        return new PageResult(qo.getCurrentPage(),qo.getPageSize(),rows,data);
    }
}
