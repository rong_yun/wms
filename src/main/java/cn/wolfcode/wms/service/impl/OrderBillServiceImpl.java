package cn.wolfcode.wms.service.impl;

import cn.wolfcode.wms.domain.OrderBill;
import cn.wolfcode.wms.domain.OrderBillItem;
import cn.wolfcode.wms.mapper.OrderBillItemMapper;
import cn.wolfcode.wms.mapper.OrderBillMapper;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IOrderBillService;
import cn.wolfcode.wms.util.PageResult;
import cn.wolfcode.wms.util.UserContect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

@Service
public class OrderBillServiceImpl implements IOrderBillService {
    @Autowired
    private OrderBillMapper orderBillMapper;
    @Autowired
    private OrderBillItemMapper orderBillItemMapper;

    public void saveOrUpdate(OrderBill entity) {
        if(entity.getId()==null){
            // entity.setInputUser(UserContect.getCurrentUser());
            // orderBillMapper.updateByPrimaryKey(entity);
            saveBill(entity);
        }else {
            //orderBillMapper.insert(entity);
            updateBill(entity);
        }
    }
    //新增订单
    private void saveBill(OrderBill entity) {
        //1设置录入人
        entity.setInputUser(UserContect.getCurrentUser());
        //2设置录入时间
        entity.setVdate(new Date());
        //3设置状态
        entity.setStatus(OrderBill.NORMAL);
        //4定义2个变量来记录当前数据的总数和总价
        BigDecimal totalNumber=BigDecimal.ZERO;
        BigDecimal totalAmount=BigDecimal.ZERO;
        for (OrderBillItem item:entity.getItems()){
            //计算单据的小计: 单价*数量    (要求四舍五入,保留2位小数)
            BigDecimal costPrice = item.getCostPrice();
            BigDecimal number = item.getNumber();
            //计算小计
            BigDecimal amount =costPrice.multiply(number).setScale(2, RoundingMode.HALF_UP);
            //设置小计
            item.setAmount(amount);
            //叠加总数和总价
            totalNumber=totalNumber.add(number);
            totalAmount=totalAmount.add(amount);
        }
        //设置总数和总价
        entity.setTotalNumber(totalNumber);
        entity.setTotalAmount(totalAmount);
        //保存订单
        orderBillMapper.insert(entity);
        //保存明细
        for(OrderBillItem item:entity.getItems()){
            item.setBillId(entity.getId());
            //保存到数据库
            orderBillItemMapper.insert(item);
        }
    }
    //更新订单
    private void updateBill(OrderBill entity) {
        //根据Id查询完整的单据信息
        OrderBill old = orderBillMapper.selectByPrimaryKey(entity.getId());
        //检查单据是否审核
        if (old.getStatus() == OrderBill.NORMAL) {
            //删除当前的单据明细
            orderBillMapper.deleteItemByBillId(old.getId());

            //定义变量来记录总数和总价
            BigDecimal totalNumber = BigDecimal.ZERO;
            BigDecimal totalAmount = BigDecimal.ZERO;
            //计算总价和总数
            for (OrderBillItem item : entity.getItems()) {
                BigDecimal costPrice = item.getCostPrice();
                BigDecimal number = item.getNumber();
                //计算小计
                BigDecimal amount = costPrice.multiply(number).setScale(2, RoundingMode.HALF_UP);
                //设置小计
                item.setAmount(amount);
                //叠加总数和总价
                totalNumber = totalNumber.add(number);
                totalAmount = totalAmount.add(amount);
                //保存明细
                item.setBillId(old.getId());
                orderBillItemMapper.insert(item);
            }
            old.setTotalNumber(totalNumber);
            old.setTotalAmount(totalAmount);
            //回填数据
            old.setSn(entity.getSn());
            old.setVdate(entity.getVdate());
            old.setSupplier(entity.getSupplier());
            //更新订单
            orderBillMapper.updateByPrimaryKey(old);
        }
    }
    public void delete(Long id) {
        //根据Id查询完整的单据信息
        OrderBill old = orderBillMapper.selectByPrimaryKey(id);
        //检查单据是否审核
        if (old.getStatus()==OrderBill.NORMAL) {
            //删除当前的单据明细
            orderBillMapper.deleteItemByBillId(id);
            orderBillMapper.deleteByPrimaryKey(id);
    }}

    public OrderBill get(Long id) {
        return orderBillMapper.selectByPrimaryKey(id);
    }
    public PageResult query(QueryObject qo) {
        //查询总记录数
        Integer rows=orderBillMapper.queryForCount(qo);
        if(rows==0){
            return PageResult.EMPTY_PAGE;
        }
        //查询列表
        List<?> data=orderBillMapper.queryForList(qo);
        return new PageResult(qo.getCurrentPage(),qo.getPageSize(),rows,data);
    }

    @Override
    public void audit(Long id) {
        //根据Id查询完整的单据信息
        OrderBill old = orderBillMapper.selectByPrimaryKey(id);
        //检查单据是否审核
        if (old.getStatus() == OrderBill.NORMAL) {//判断订单是否审核
            //核心:填入审核人,时间,状态
            old.setAuditor(UserContect.getCurrentUser());//设置审核人
            old.setAuditTime(new Date());//设置审核时间
            old.setStatus(OrderBill.AUDIT);//设置审核状态
            orderBillMapper.audit(old);//执行方法
        }
    }
}
