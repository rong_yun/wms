package cn.wolfcode.wms.service.impl;

import cn.wolfcode.wms.domain.*;
import cn.wolfcode.wms.mapper.ProductStockMapper;
import cn.wolfcode.wms.mapper.StockIncomeBillItemMapper;
import cn.wolfcode.wms.mapper.StockIncomeBillMapper;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IStockIncomeBillService;
import cn.wolfcode.wms.util.PageResult;
import cn.wolfcode.wms.util.UserContect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

@Service
public class StockIncomeBillServiceImpl implements IStockIncomeBillService {
    @Autowired
    private StockIncomeBillMapper stockIncomeBillMapper;
    @Autowired
    private StockIncomeBillItemMapper stockIncomeBillItemMapper;
    @Autowired
    private ProductStockMapper productStockMapper;

    public void saveOrUpdate(StockIncomeBill entity) {
        if(entity.getId()==null){
            // entity.setInputUser(UserContect.getCurrentUser());
            // stockIncomeBillMapper.updateByPrimaryKey(entity);
            saveBill(entity);
        }else {
            //stockIncomeBillMapper.insert(entity);
            updateBill(entity);
        }
    }
    //新增订单
    private void saveBill(StockIncomeBill entity) {
        //1设置录入人
        entity.setInputUser(UserContect.getCurrentUser());
        //2设置录入时间
        entity.setVdate(new Date());
        //3设置状态
        entity.setStatus(StockIncomeBill.NORMAL);
        //4定义2个变量来记录当前数据的总数和总价
        BigDecimal totalNumber=BigDecimal.ZERO;
        BigDecimal totalAmount=BigDecimal.ZERO;
        for (StockIncomeBillItem item:entity.getItems()){
            //计算单据的小计: 单价*数量    (要求四舍五入,保留2位小数)
            BigDecimal costPrice = item.getCostPrice();
            BigDecimal number = item.getNumber();
            //计算小计
            BigDecimal amount =costPrice.multiply(number).setScale(2, RoundingMode.HALF_UP);
            //设置小计
            item.setAmount(amount);
            //叠加总数和总价
            totalNumber=totalNumber.add(number);
            totalAmount=totalAmount.add(amount);
        }
        //设置总数和总价
        entity.setTotalNumber(totalNumber);
        entity.setTotalAmount(totalAmount);
        //保存订单
        stockIncomeBillMapper.insert(entity);
        //保存明细
        for(StockIncomeBillItem item:entity.getItems()){
            item.setBillId(entity.getId());
            //保存到数据库
            stockIncomeBillItemMapper.insert(item);
        }
    }
    //更新订单
    private void updateBill(StockIncomeBill entity) {
        //根据Id查询完整的单据信息
        StockIncomeBill old = stockIncomeBillMapper.selectByPrimaryKey(entity.getId());
        //检查单据是否审核
        if (old.getStatus() == StockIncomeBill.NORMAL) {
            //删除当前的单据明细
            stockIncomeBillMapper.deleteItemByBillId(old.getId());

            //定义变量来记录总数和总价
            BigDecimal totalNumber = BigDecimal.ZERO;
            BigDecimal totalAmount = BigDecimal.ZERO;
            //计算总价和总数
            for (StockIncomeBillItem item : entity.getItems()) {
                BigDecimal costPrice = item.getCostPrice();
                BigDecimal number = item.getNumber();
                //计算小计
                BigDecimal amount = costPrice.multiply(number).setScale(2, RoundingMode.HALF_UP);
                //设置小计
                item.setAmount(amount);
                //叠加总数和总价
                totalNumber = totalNumber.add(number);
                totalAmount = totalAmount.add(amount);
                //保存明细
                item.setBillId(old.getId());
                stockIncomeBillItemMapper.insert(item);
            }
            old.setTotalNumber(totalNumber);
            old.setTotalAmount(totalAmount);
            //回填数据
            old.setSn(entity.getSn());
            old.setVdate(entity.getVdate());
            old.setDepot(entity.getDepot());
            //更新订单
            stockIncomeBillMapper.updateByPrimaryKey(old);
        }
    }
    public void delete(Long id) {
        //根据Id查询完整的单据信息
        StockIncomeBill old = stockIncomeBillMapper.selectByPrimaryKey(id);
        //检查单据是否审核
        if (old.getStatus()==StockIncomeBill.NORMAL) {
            //删除当前的单据明细
            stockIncomeBillMapper.deleteItemByBillId(id);
            stockIncomeBillMapper.deleteByPrimaryKey(id);
    }}

    public StockIncomeBill get(Long id) {
        return stockIncomeBillMapper.selectByPrimaryKey(id);
    }
    public PageResult query(QueryObject qo) {
        //查询总记录数
        Integer rows=stockIncomeBillMapper.queryForCount(qo);
        if(rows==0){
            return PageResult.EMPTY_PAGE;
        }
        //查询列表
        List<?> data=stockIncomeBillMapper.queryForList(qo);
        return new PageResult(qo.getCurrentPage(),qo.getPageSize(),rows,data);
    }

    //入库审核
    public void audit(Long id) {
        //根据Id查询完整的单据信息
        StockIncomeBill bill = stockIncomeBillMapper.selectByPrimaryKey(id);
        //检查单据是否审核
        if (bill.getStatus() == StockIncomeBill.NORMAL) {
            Depot depot = bill.getDepot();
            //遍历所有明细依次入库
            for (StockIncomeBillItem item : bill.getItems()) {
                Product product=item.getProduct();
                //检查仓库中是否已存在该商品
                ProductStock ps = productStockMapper
                        .selectByProductAndDepot(product.getId(), depot.getId());
                if (ps!=null) {
                    //使用激动加权平均重写计算库存价格
                    BigDecimal amount=item.getAmount().add(ps.getAmount());
                    BigDecimal number = item.getNumber().add(ps.getStoreNumber());
                    BigDecimal price = amount.divide(number, 2, RoundingMode.HALF_UP);
                    //更新数据库
                    ps.setPrice(price);
                    ps.setStoreNumber(number);
                    ps.setAmount(amount);
                    productStockMapper.updateByPrimaryKey(ps);
                }else {
                    //直接创建一个库存对象,存入数据
                    ps=new ProductStock();
                    ps.setPrice(item.getCostPrice());
                    ps.setStoreNumber(item.getNumber());
                    ps.setAmount(item.getAmount());
                    ps.setProduct(product);
                    ps.setDepot(depot);
                    productStockMapper.insert(ps);
                }
            }
            //设置时间,审核人和状态
            bill.setAuditTime(new Date());
            bill.setAuditor(UserContect.getCurrentUser());
            bill.setStatus(StockIncomeBill.AUDIT);
            stockIncomeBillMapper.audit(bill);
        }
    }
}
