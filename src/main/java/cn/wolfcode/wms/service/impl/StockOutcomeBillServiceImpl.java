package cn.wolfcode.wms.service.impl;

import cn.wolfcode.wms.domain.*;
import cn.wolfcode.wms.mapper.ProductStockMapper;
import cn.wolfcode.wms.mapper.SaleAcCountMapper;
import cn.wolfcode.wms.mapper.StockOutcomeBillItemMapper;
import cn.wolfcode.wms.mapper.StockOutcomeBillMapper;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IStockOutcomeBillService;
import cn.wolfcode.wms.util.PageResult;
import cn.wolfcode.wms.util.UserContect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

@Service
public class StockOutcomeBillServiceImpl implements IStockOutcomeBillService {
    @Autowired
    private StockOutcomeBillMapper stockOutcomeBillMapper;
    @Autowired
    private StockOutcomeBillItemMapper stockOutcomeBillItemMapper;
    @Autowired
    private ProductStockMapper productStockMapper;
    @Autowired
    private SaleAcCountMapper saleAcCountMapper;

    public void saveOrUpdate(StockOutcomeBill entity) {
        if(entity.getId()==null){
            // entity.setInputUser(UserContect.getCurrentUser());
            // stockOutcomeBillMapper.updateByPrimaryKey(entity);
            saveBill(entity);
        }else {
            //stockOutcomeBillMapper.insert(entity);
            updateBill(entity);
        }
    }
    //新增订单
    private void saveBill(StockOutcomeBill entity) {
        //1设置录入人
        entity.setInputUser(UserContect.getCurrentUser());
        //2设置录入时间
        entity.setVdate(new Date());
        //3设置状态
        entity.setStatus(StockOutcomeBill.NORMAL);
        //4定义2个变量来记录当前数据的总数和总价
        BigDecimal totalNumber=BigDecimal.ZERO;
        BigDecimal totalAmount=BigDecimal.ZERO;
        for (StockOutcomeBillItem item:entity.getItems()){
            //计算单据的小计: 单价*数量    (要求四舍五入,保留2位小数)
            BigDecimal salePrice = item.getSalePrice();
            BigDecimal number = item.getNumber();
            //计算小计
            BigDecimal amount =salePrice.multiply(number).setScale(2, RoundingMode.HALF_UP);
            //设置小计
            item.setAmount(amount);
            //叠加总数和总价
            totalNumber=totalNumber.add(number);
            totalAmount=totalAmount.add(amount);
        }
        //设置总数和总价
        entity.setTotalNumber(totalNumber);
        entity.setTotalAmount(totalAmount);
        //保存订单
        stockOutcomeBillMapper.insert(entity);
        //保存明细
        for(StockOutcomeBillItem item:entity.getItems()){
            item.setBillId(entity.getId());
            //保存到数据库
            stockOutcomeBillItemMapper.insert(item);
        }
    }
    //更新订单
    private void updateBill(StockOutcomeBill entity) {
        //根据Id查询完整的单据信息
        StockOutcomeBill old = stockOutcomeBillMapper.selectByPrimaryKey(entity.getId());
        //检查单据是否审核
        if (old.getStatus() == StockOutcomeBill.NORMAL) {
            //删除当前的单据明细
            stockOutcomeBillMapper.deleteItemByBillId(old.getId());

            //定义变量来记录总数和总价
            BigDecimal totalNumber = BigDecimal.ZERO;
            BigDecimal totalAmount = BigDecimal.ZERO;
            //计算总价和总数
            for (StockOutcomeBillItem item : entity.getItems()) {
                BigDecimal salePrice = item.getSalePrice();
                BigDecimal number = item.getNumber();
                //计算小计
                BigDecimal amount = salePrice.multiply(number).setScale(2, RoundingMode.HALF_UP);
                //设置小计
                item.setAmount(amount);
                //叠加总数和总价
                totalNumber = totalNumber.add(number);
                totalAmount = totalAmount.add(amount);
                //保存明细
                item.setBillId(old.getId());
                stockOutcomeBillItemMapper.insert(item);
            }
            old.setTotalNumber(totalNumber);
            old.setTotalAmount(totalAmount);
            //回填数据
            old.setSn(entity.getSn());
            old.setVdate(entity.getVdate());
            old.setDepot(entity.getDepot());
            //更新订单
            stockOutcomeBillMapper.updateByPrimaryKey(old);
        }
    }
    public void delete(Long id) {
        //根据Id查询完整的单据信息
        StockOutcomeBill old = stockOutcomeBillMapper.selectByPrimaryKey(id);
        //检查单据是否审核
        if (old.getStatus()==StockOutcomeBill.NORMAL) {
            //删除当前的单据明细
            stockOutcomeBillMapper.deleteItemByBillId(id);
            stockOutcomeBillMapper.deleteByPrimaryKey(id);
    }}

    public StockOutcomeBill get(Long id) {
        return stockOutcomeBillMapper.selectByPrimaryKey(id);
    }
    public PageResult query(QueryObject qo) {
        //查询总记录数
        Integer rows=stockOutcomeBillMapper.queryForCount(qo);
        if(rows==0){
            return PageResult.EMPTY_PAGE;
        }
        //查询列表
        List<?> data=stockOutcomeBillMapper.queryForList(qo);
        return new PageResult(qo.getCurrentPage(),qo.getPageSize(),rows,data);
    }

    //出库审核
    public void audit(Long id) {
        //根据Id查询完整的单据信息
        StockOutcomeBill bill = stockOutcomeBillMapper.selectByPrimaryKey(id);
        //检查单据是否审核
        if (bill.getStatus() == StockOutcomeBill.NORMAL) {
            Depot depot = bill.getDepot();
            //遍历所有明细依次出库
            for (StockOutcomeBillItem item : bill.getItems()) {
                Product product=item.getProduct();
                //出库操作
                //1查询仓库中是否有该商品
                ProductStock ps = productStockMapper.selectByProductAndDepot(product.getId(), depot.getId());
                //2在仓库中有无库存
                if (ps==null) {
                    String msg="仓库["+depot.getName()+"]没有该商品["+product.getName()+"]的库存";
                    throw new RuntimeException(msg);
                }
                //3检查库存是否满足出库数量
                if (item.getNumber().compareTo(ps.getStoreNumber())>0) {
                    String msg="仓库["+depot.getName()+"]该商品["+product.getName()+"]的库存数量是"
                            +ps.getStoreNumber()+"不足"+item.getNumber()+"出库的需求";
                    throw new RuntimeException(msg);
                }
                //4满足出库条件,减少总价值和总数
                BigDecimal amount = ps.getPrice().multiply(item.getNumber())
                        .setScale(2, RoundingMode.HALF_UP);
                amount=ps.getAmount().subtract(amount);
                ps.setAmount(amount);
                BigDecimal num = ps.getStoreNumber().subtract(item.getNumber());
                ps.setStoreNumber(num);
               //5更新仓库
                productStockMapper.updateByPrimaryKey(ps);
                //记录销售账单
                SaleAcCount sa=new SaleAcCount();
                sa.setVdate(new Date());
                sa.setCostPrice(ps.getPrice());
                sa.setNumber(item.getNumber());
                sa.setCostAmount(amount);
                sa.setSalePrice(item.getSalePrice());
                sa.setCostAmount(item.getSalePrice().multiply(item.getNumber())
                        .setScale(2,RoundingMode.HALF_UP));
                sa.setProduct(product);
                sa.setSaleMan(bill.getInputUser());
                sa.setClient(bill.getClient());
                saleAcCountMapper.insert(sa);
            }
            //设置审核人,审核时间,状态
            bill.setAuditTime(new Date());
            bill.setAuditor(UserContect.getCurrentUser());
            bill.setStatus(StockIncomeBill.AUDIT);
            stockOutcomeBillMapper.audit(bill);
        }
    }
}
