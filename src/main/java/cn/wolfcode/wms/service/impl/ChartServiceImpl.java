package cn.wolfcode.wms.service.impl;

import cn.wolfcode.wms.mapper.ChartMapper;
import cn.wolfcode.wms.query.OrderChartQueryObject;
import cn.wolfcode.wms.query.SaleChartQueryObject;
import cn.wolfcode.wms.service.IChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ChartServiceImpl implements IChartService {
    @Autowired
    private ChartMapper chartMapper;

    @Override
    public List<Map<String, Object>> queryOrderChart(OrderChartQueryObject qo) {
        return chartMapper.queryOrderChart(qo);
    }

    @Override
    public List<Map<String, Object>> querySaleChart(SaleChartQueryObject qo) {
        return chartMapper.querySaleChart(qo);
    }
}
