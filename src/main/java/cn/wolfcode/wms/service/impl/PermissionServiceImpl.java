package cn.wolfcode.wms.service.impl;

import cn.wolfcode.wms.domain.Permission;
import cn.wolfcode.wms.mapper.PermissionMapper;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IPermissionService;
import cn.wolfcode.wms.util.PageResult;
import cn.wolfcode.wms.util.PermissionUtil;
import cn.wolfcode.wms.util.RequiredPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;

@Service
public class PermissionServiceImpl implements IPermissionService {
    @Autowired
    private PermissionMapper permissionMapper;
    @Autowired
    private ApplicationContext ctx;

    //加载权限
    public void reload() {
        //查询数据库中所有的权限
        List<String> exps=permissionMapper.selectAllExpression();
        //从容器中找到所有的控制器,迭代出每一个方法
        Collection<Object> ctrls = ctx.getBeansWithAnnotation(Controller.class).values();
        for (Object ctr : ctrls) {
            Method[] ms = ctr.getClass().getDeclaredMethods();
            for (Method m : ms) {
                String exp = PermissionUtil.buildexpression(m);
                RequiredPermission anno= m.getAnnotation(RequiredPermission.class);
                //判断每一个方法是否需要生成权限
                if(anno != null&& !exps.contains(exp)){
                    //生成权限对象
                    Permission p=new Permission();
                    p.setName(anno.value());
                    System.out.println("加载"+p);
                    p.setExpression(exp);
                    //保存到数据库
                    permissionMapper.insert(p);
                }
            }
        }
    }

    public void delete(Long id) {
            permissionMapper.deleteByPrimaryKey(id);
    }

    public List<Permission> list() {
        return permissionMapper.selectAll();
    }

    public PageResult query(QueryObject qo) {
        //查询总记录数
        Integer rows=permissionMapper.queryForCount(qo);
        if(rows==0){
            return PageResult.EMPTY_PAGE;
        }
        //查询列表
        List<?> data=permissionMapper.queryForList(qo);
        return new PageResult(qo.getCurrentPage(),qo.getPageSize(),rows,data);
    }


}
