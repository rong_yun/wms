package cn.wolfcode.wms.service.impl;

import cn.wolfcode.wms.domain.Employee;
import cn.wolfcode.wms.mapper.EmployeeMapper;
import cn.wolfcode.wms.mapper.PermissionMapper;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IEmployeeService;
import cn.wolfcode.wms.util.MD5;
import cn.wolfcode.wms.util.PageResult;
import cn.wolfcode.wms.util.UserContect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class EmployeeServiceImpl implements IEmployeeService {
    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private PermissionMapper permissionMapper;
    public void saveOrUpdate(Employee entity, Long[] ids) {
        if(entity.getId()!=null){
            employeeMapper.deleteRelation(entity.getId());
            employeeMapper.updateByPrimaryKey(entity);
        }else {
            //对用户密码进行加密
            String newPwd = MD5.encoder(entity.getPassword());
            entity.setPassword(newPwd);
            employeeMapper.insert(entity);
        }
        //处理关联关系
        if(ids!=null){
            for (Long roleId : ids) {
                employeeMapper.insertRelation(entity.getId(),roleId);
            }
        }
    }

    public void delete(Long id) {
            employeeMapper.deleteRelation(id);
            employeeMapper.deleteByPrimaryKey(id);
    }

    public Employee get(Long id) {
        return employeeMapper.selectByPrimaryKey(id);
    }

    public PageResult query(QueryObject qo) {
        //查询总记录数
        Integer rows=employeeMapper.queryForCount(qo);
        if(rows==0){
            return PageResult.EMPTY_PAGE;
        }

        //查询列表
        List<?> data=employeeMapper.queryForList(qo);
        return new PageResult(qo.getCurrentPage(),qo.getPageSize(),rows,data);
    }

    //登录
    public void login(String username, String password) {
        //将登录的用户封装成一个Employee对象,并对密码进行加密
        Employee em=employeeMapper.selectByInfo(username,password /*MD5.encoder(password)*/);
        System.out.println(em);
        if(em==null){
            throw new RuntimeException("账号密码不匹配");
        }
        //把对象传入session
        UserContect.setCurrentUser(em);
        //判断是否是超级用户,不是则将权限也存入session
        if (!em.isAdmin()) {
            List<String> exps=permissionMapper.selectByEmployeeId(em.getId());
            UserContect.setExpression(exps);
        }
    }

    //批量删除
    public void batchDelete(List<Long> ids) {
        employeeMapper.batchDelete(ids);
    }
}

