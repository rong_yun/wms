package cn.wolfcode.wms.service.impl;

import cn.wolfcode.wms.domain.SystemMenu;
import cn.wolfcode.wms.mapper.SystemMenuMapper;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.query.SystemMenuQueryObject;
import cn.wolfcode.wms.service.ISystemMenuService;
import cn.wolfcode.wms.util.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class SystemMenuServiceImpl implements ISystemMenuService {
    @Autowired
    private SystemMenuMapper systemMenuMapper;
    public void saveOrUpdate(SystemMenu entity) {
        if(entity.getId()!=null){
            systemMenuMapper.updateByPrimaryKey(entity);
        }else {
            systemMenuMapper.insert(entity);
        }
    }

    public void delete(Long id) {
        systemMenuMapper.deleteChildMenu(id);
            systemMenuMapper.deleteByPrimaryKey(id);
    }

    public SystemMenu get(Long id) {
        return systemMenuMapper.selectByPrimaryKey(id);
    }

    public List<SystemMenu> list() {
        return systemMenuMapper.selectAll();
    }

    @Override
    public List<SystemMenu> query(SystemMenuQueryObject qo) {
        return systemMenuMapper.query(qo);
    }

    @Override
    public List<SystemMenu> getParentMenus(Long menuId) {
        List<SystemMenu> menus=new ArrayList<>();
        SystemMenu menu = systemMenuMapper.selectByPrimaryKey(menuId);
        if (menu!=null) {
            menus.add(menu);
            menu.getParent();
            while (menu != null) {
                menus.add(menu);
                menu=menu.getParent();
            }
        }
        //逆序输出
        Collections.reverse(menus);
        return menus;
    }

    //获取menu
    public List<Map<String, Object>> getMenusBySn(String menuSn) {
        return systemMenuMapper.selectMenuBySn(menuSn);
    }

}
