package cn.wolfcode.wms.service;

import cn.wolfcode.wms.domain.OrderBill;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.util.PageResult;

public interface IOrderBillService {
    void saveOrUpdate(OrderBill entity);
    void delete(Long id);
    OrderBill get(Long id);
    PageResult query(QueryObject qo);
    //审核
    void audit(Long id);
}
