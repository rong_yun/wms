package cn.wolfcode.wms.web.interceptor;

import cn.wolfcode.wms.domain.Employee;
import cn.wolfcode.wms.util.PermissionUtil;
import cn.wolfcode.wms.util.RequiredPermission;
import cn.wolfcode.wms.util.SecurityException;
import cn.wolfcode.wms.util.UserContect;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.List;

public class SecurityInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        Employee exp = UserContect.getCurrentUser();
        //判断是不是超级管理员
        if (exp.isAdmin()){
            return true;
        }
        HandlerMethod hm= (HandlerMethod) handler;
        Method m = hm.getMethod();
        if(!m.isAnnotationPresent(RequiredPermission.class)){
            return true;
        }
        List<String> expression = UserContect.getExpression();
        if (expression.contains(PermissionUtil.buildexpression(m))) {
            return true;
        }
       throw new SecurityException();
    }
}
