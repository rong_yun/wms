package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.SystemMenu;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.query.SystemMenuQueryObject;
import cn.wolfcode.wms.service.ISystemMenuService;
import cn.wolfcode.wms.util.JSONResult;
import cn.wolfcode.wms.util.RequiredPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("systemMenu")
public class SystemMenuController {
    @Autowired
    private ISystemMenuService systemMenuService;

    @RequestMapping("list")
    public String list(@ModelAttribute("qo") SystemMenuQueryObject qo, Model model)throws Exception{
        //显示当前菜单
        if (qo.getParentId()!=null) {
           List<SystemMenu> menus= systemMenuService.getParentMenus(qo.getParentId());
            model.addAttribute("menus",menus);
        }
        //model.addAttribute("list",systemMenuService.list());
        List<SystemMenu> list = systemMenuService.query(qo);
        System.out.println(list);
        model.addAttribute("list",systemMenuService.query(qo));
        return "systemMenu/list";
    }

    @RequestMapping("input")
    public String input(Long id,Model model,Long parentId)throws Exception{
        if(id!=null){
            model.addAttribute("entity",systemMenuService.get(id));
        }
        System.out.println("这是个数组吗"+parentId);
        if(parentId!=null){
            model.addAttribute("parent",systemMenuService.get(parentId));
        }

        return "systemMenu/input";
    }

    @RequestMapping("delete")
    @ResponseBody
    public Object delete(Long id)throws Exception{
       if(id!=null){
           systemMenuService.delete(id);
       }
        return new JSONResult();
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(SystemMenu entity)throws Exception{
        systemMenuService.saveOrUpdate(entity);
        return new JSONResult();
    }
    @RequestMapping("getMenusBySn")
    @ResponseBody
    public Object getMenusBySn(String menuSn)throws Exception{
        List<Map<String,Object>> list=systemMenuService.getMenusBySn(menuSn);
        return list;
       // return new JSONResult();
    }
}
