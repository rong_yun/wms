package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.Brand;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IBrandService;
import cn.wolfcode.wms.util.JSONResult;
import cn.wolfcode.wms.util.RequiredPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("brand")
public class BrandController {
    @Autowired
    private IBrandService brandService;

    @RequestMapping("list")
    @RequiredPermission("部门列表")
    public String list(@ModelAttribute("qo") QueryObject qo , Model model)throws Exception{
        model.addAttribute("result",brandService.query(qo));
        return "brand/list";
    }

    @RequestMapping("input")
    @RequiredPermission("部门编辑")
    public String input(Long id,Model model)throws Exception{
        if(id!=null){
            model.addAttribute("entity",brandService.get(id));
        }
        return "brand/input";
    }

    @RequestMapping("delete")
    @ResponseBody
    @RequiredPermission("部门删除")
    public Object delete(Long id)throws Exception{
       if(id!=null){
           brandService.delete(id);
       }
        return new JSONResult();
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(Brand entity)throws Exception{
        brandService.saveOrUpdate(entity);
        return new JSONResult();
        //"redirect:/brand/list.do";
    }
}
