package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.Brand;
import cn.wolfcode.wms.domain.Product;
import cn.wolfcode.wms.query.ProductQueryObject;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IBrandService;
import cn.wolfcode.wms.service.IProductService;
import cn.wolfcode.wms.util.JSONResult;
import cn.wolfcode.wms.util.RequiredPermission;
import cn.wolfcode.wms.util.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;

@Controller
@RequestMapping("product")
public class ProductController {
    @Autowired
    private IProductService productService;
    @Autowired
    private IBrandService brandService;
    @Autowired
    private ServletContext ctx;

    @RequestMapping("list")
    @RequiredPermission("商品列表")
    public String list(@ModelAttribute("qo") ProductQueryObject qo , Model model)throws Exception{
        model.addAttribute("result",productService.query(qo));
        model.addAttribute("brands",brandService.list());
        return "product/list";
    }
    @RequestMapping("productListViews")
    @RequiredPermission("商品列表")
    public String productListViews(@ModelAttribute("qo") ProductQueryObject qo , Model model)throws Exception{
        model.addAttribute("result",productService.query(qo));
        model.addAttribute("brands",brandService.list());
        return "product/productListViews";
    }


    @RequestMapping("input")
    @RequiredPermission("商品编辑")
    public String input(Long id,Model model)throws Exception{
        if(id!=null){
            model.addAttribute("entity",productService.get(id));
        }
        model.addAttribute("brands",brandService.list());
        return "product/input";
    }

    @RequestMapping("delete")
    @ResponseBody
    @RequiredPermission("商品删除")
    public Object delete(Long id,String imagePath)throws Exception{
       if(id!=null){
           productService.delete(id);
           if(StringUtils.hasLength(imagePath)){
               UploadUtil.deleteFile(ctx,imagePath);

           }
       }
        return new JSONResult();
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(Product entity, MultipartFile pic)throws Exception{
        //处理上传的文件
        if(pic!=null&&pic.getSize()>0&&pic.getContentType().startsWith("image/")){
            //判断图片的路径是否为空
            if (entity.getImagePath()!=null) {
                //不为空执行删除旧文件
                UploadUtil.deleteFile(ctx,entity.getImagePath());
            }
            //把文件保存到服务器中
            String dir = ctx.getRealPath("/upload");//获取到文件的路径
            String imagePath = UploadUtil.upload(pic, dir);//获取到文件及文件的路径
            //把图片在服务器中的路径保存到对象中
            entity.setImagePath(imagePath);
        }
        Brand brand = brandService.get(entity.getBrandId());//获取到品牌
        entity.setBrandName(brand.getName());//设置品牌名称
        productService.saveOrUpdate(entity);//添加或更新对象
        return new JSONResult();//返回一个json对象
        //"redirect:/product/list.do";
    }
}
