package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.Permission;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IPermissionService;
import cn.wolfcode.wms.util.JSONResult;
import cn.wolfcode.wms.util.RequiredPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("permission")
public class PermissionController {
    @Autowired
    private IPermissionService permissionService;

    @RequestMapping("list")
    @RequiredPermission("权限列表")
    public String list(@ModelAttribute("qo") QueryObject qo , Model model)throws Exception{
        model.addAttribute("result",permissionService.query(qo));
        return "permission/list";
    }

    //删除权限
    @RequestMapping("delete")
    @ResponseBody
    @RequiredPermission("权限删除")
    public Object delete(Long id)throws Exception{
       if(id!=null){
           permissionService.delete(id);
       }
        return new JSONResult();
    }

    //加载权限
    @RequiredPermission("权限加载")
    @RequestMapping("reload")
    @ResponseBody
    public Object reload(){
        permissionService.reload();
        return new JSONResult();
    }

}
