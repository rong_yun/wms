package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.Employee;
import cn.wolfcode.wms.query.EmployeeQueryObject;
import cn.wolfcode.wms.service.IDepartmentService;
import cn.wolfcode.wms.service.IEmployeeService;
import cn.wolfcode.wms.service.IRoleService;
import cn.wolfcode.wms.util.JSONResult;
import cn.wolfcode.wms.util.RequiredPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;

@Controller
@RequestMapping("employee")
public class EmployeeController {
    @Autowired
    private IEmployeeService employeeService;
    @Autowired
    private IDepartmentService departmentService;
    @Autowired
    private IRoleService roleService;

    @RequestMapping("list")
    @RequiredPermission("员工列表")
    public String list(@ModelAttribute("qo") EmployeeQueryObject qo, Model model)throws Exception{
        model.addAttribute("result",employeeService.query(qo));
        model.addAttribute("depts",departmentService.list());
        return "employee/list";
    }
    @RequiredPermission("员工编辑")
    @RequestMapping("input")
    public String input(Long id,Model model)throws Exception{
        if(id!=null){
            model.addAttribute("entity",employeeService.get(id));
        }
        model.addAttribute("depts",departmentService.list());
        model.addAttribute("role",roleService.list());
        return "employee/input";
    }
    @RequiredPermission("员工删除")
    @RequestMapping("delete")
    @ResponseBody
    public Object delete(Long id)throws Exception{
       if(id!=null){
           employeeService.delete(id);
       }
        return new JSONResult();
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public Object  saveOrUpdate(Employee entity ,Long[] ids)throws Exception{
        employeeService.saveOrUpdate(entity,ids);
        return new JSONResult();
    }
    //批量删除
    @RequestMapping("batchDelete")
    @ResponseBody
    public Object batchDelete(Long [] ids){
        if (ids!=null){
        System.out.println("测试:"+ Arrays.toString(ids));
        employeeService.batchDelete(Arrays.asList(ids));}//通过Arrays.asList方法遍历出所有的ids
        return new JSONResult();
    }
}
