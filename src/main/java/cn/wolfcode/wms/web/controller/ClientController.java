package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.Client;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IClientService;
import cn.wolfcode.wms.util.JSONResult;
import cn.wolfcode.wms.util.RequiredPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("client")
public class ClientController {
    @Autowired
    private IClientService clientService;

    @RequestMapping("list")
    @RequiredPermission("客户列表")
    public String list(@ModelAttribute("qo") QueryObject qo , Model model)throws Exception{
        model.addAttribute("result",clientService.query(qo));
        return "client/list";
    }

    @RequestMapping("input")
    @RequiredPermission("客户编辑")
    public String input(Long id,Model model)throws Exception{
        if(id!=null){
            model.addAttribute("entity",clientService.get(id));
        }
        return "client/input";
    }

    @RequestMapping("delete")
    @ResponseBody
    @RequiredPermission("删除")
    public Object delete(Long id)throws Exception{
       if(id!=null){
           clientService.delete(id);
       }
        return new JSONResult();
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(Client entity)throws Exception{
        clientService.saveOrUpdate(entity);
        return new JSONResult();
        //"redirect:/client/list.do";
    }
}
