package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.query.OrderChartQueryObject;
import cn.wolfcode.wms.query.SaleChartQueryObject;
import cn.wolfcode.wms.service.IBrandService;
import cn.wolfcode.wms.service.IChartService;
import cn.wolfcode.wms.service.IClientService;
import cn.wolfcode.wms.service.ISupplierService;
import cn.wolfcode.wms.util.Dictionary;
import cn.wolfcode.wms.util.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("chart")
public class ChartController {
    @Autowired
    private IChartService chartService;
    @Autowired
    private ISupplierService supplierService;
    @Autowired
    private IBrandService brandService;
    @Autowired
    private IClientService clientService;

    @RequestMapping("order")//报表查询
    public String list(@ModelAttribute("qo") OrderChartQueryObject qo , Model model)throws Exception{
        model.addAttribute("list",chartService.queryOrderChart(qo));
        model.addAttribute("suppliers",supplierService.list());
        model.addAttribute("brands",brandService.list());
        return "chart/order";
    }

    @RequestMapping("sale")
    public String list(@ModelAttribute("qo") SaleChartQueryObject qo , Model model)throws Exception{
        model.addAttribute("list",chartService.querySaleChart(qo));
        model.addAttribute("clients",clientService.list());
        model.addAttribute("brands",brandService.list());
        return "chart/sale";
    }
    @RequestMapping("saleByBar")
    public String saleByBar(@ModelAttribute("qo") SaleChartQueryObject qo , Model model)throws Exception{
        List<Object> x=new ArrayList<>();
        List<Object> y=new ArrayList<>();
        List<Map<String, Object>> maps = chartService.querySaleChart(qo);
        for (Map<String, Object> map : maps) {
            //获取x和y对应的值
            x.add(map.get("groupType"));
            y.add(map.get("totalAmount"));
        }
        //转换成json
        model.addAttribute("groupType", Dictionary.SALE_MAP.get(qo.getGroupType()));
        model.addAttribute("x", JSONUtil.toJSONString(x));
        model.addAttribute("y", JSONUtil.toJSONString(y));
        return "chart/saleByBar";
    }
    @RequestMapping("saleByPic")
    public String saleByPic(@ModelAttribute("qo") SaleChartQueryObject qo , Model model)throws Exception{
        List<Object> x=new ArrayList<>();
        List<Object> datas=new ArrayList<>();
        List<Map<String, Object>> maps = chartService.querySaleChart(qo);
        for (Map<String, Object> map : maps) {
            //获取x和y对应的值
            x.add(map.get("groupType"));
            Map<String,Object> obj=new HashMap<>();
            obj.put("name",map.get("groupType"));
            obj.put("value",map.get("totalAmount"));
            datas.add(obj);
        }
        //转换成json
        model.addAttribute("groupType", Dictionary.SALE_MAP.get(qo.getGroupType()));
        model.addAttribute("x", JSONUtil.toJSONString(x));
        model.addAttribute("datas", JSONUtil.toJSONString(datas));
        return "chart/saleByPic";
    }

}
