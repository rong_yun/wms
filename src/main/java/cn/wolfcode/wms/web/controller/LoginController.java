package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.service.IEmployeeService;
import cn.wolfcode.wms.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.logging.Handler;

@Controller
public class LoginController {
    @Autowired
    private IEmployeeService employeeService;

    @ResponseBody
    @RequestMapping("login")
    public Object login(String username,String password){
        JSONResult result=new JSONResult();
        try {
            employeeService.login(username,password);
        }catch (Exception e){
            e.printStackTrace();
            //将错误信息保存到sessiom中,通过session传入前台
            result.mark(e.getMessage());
        }
        return result;
    }

    @RequestMapping("main")
    public String main(){
        return "main";
    }
    @RequestMapping("logout")
    public String logout(HttpSession session){
        session.invalidate();//销毁session
        return "redirect:login.html";
    }
    @RequestMapping("nopermission")
    public String nopermission(HttpSession session){
        return "common/nopermission";//跳转逻辑试图
    }
}
