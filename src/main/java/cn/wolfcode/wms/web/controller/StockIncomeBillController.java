package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.StockIncomeBill;
import cn.wolfcode.wms.query.StockIncomeBillQueryObject;
import cn.wolfcode.wms.query.SupplierQueryObject;
import cn.wolfcode.wms.service.IDepotService;
import cn.wolfcode.wms.service.IStockIncomeBillService;
import cn.wolfcode.wms.service.ISupplierService;
import cn.wolfcode.wms.util.JSONResult;
import cn.wolfcode.wms.util.RequiredPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("stockIncomeBill")
public class StockIncomeBillController {
    @Autowired
    private IStockIncomeBillService stockIncomeBillService;
    @Autowired
    private IDepotService depotService;

    @RequestMapping("audit")
    @ResponseBody
    public Object audit(Long id)throws Exception{
        stockIncomeBillService.audit(id);
        return new JSONResult();
    }

    @RequestMapping("list")
    @RequiredPermission("入库列表")
    public String list(@ModelAttribute("qo") StockIncomeBillQueryObject qo , Model model)throws Exception{
        model.addAttribute("result",stockIncomeBillService.query(qo));
        model.addAttribute("depots",depotService.list());
        return "stockIncomeBill/list";
    }

    @RequestMapping("input")
    @RequiredPermission("入库编辑")
    public String input(Long id,Model model)throws Exception{
        if(id!=null){
            model.addAttribute("entity",stockIncomeBillService.get(id));
        }
        model.addAttribute("depots",depotService.list());
        return "stockIncomeBill/input";
    }
    @RequestMapping("view")
    public String view(Long id,Model model)throws Exception{
        if(id!=null){
            model.addAttribute("entity",stockIncomeBillService.get(id));
        }
        model.addAttribute("suppliers",depotService.list());
        return "stockIncomeBill/view";
    }

    @RequestMapping("delete")
    @ResponseBody
    @RequiredPermission("入库删除")
    public Object delete(Long id)throws Exception{
       if(id!=null){
           stockIncomeBillService.delete(id);
       }
        return new JSONResult();
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(StockIncomeBill entity)throws Exception{
        stockIncomeBillService.saveOrUpdate(entity);
        return new JSONResult();
        //"redirect:/stockIncomeBill/list.do";
    }
}
