package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.OrderBill;
import cn.wolfcode.wms.query.SupplierQueryObject;
import cn.wolfcode.wms.service.IOrderBillService;
import cn.wolfcode.wms.service.ISupplierService;
import cn.wolfcode.wms.util.JSONResult;
import cn.wolfcode.wms.util.RequiredPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("orderBill")
public class OrderBillController {
    @Autowired
    private IOrderBillService orderBillService;
    @Autowired
    private ISupplierService supplierService;

    @RequestMapping("audit")
    @ResponseBody
    public Object audit(Long id)throws Exception{
        orderBillService.audit(id);
        return new JSONResult();
    }

    @RequestMapping("list")
    @RequiredPermission("采购订单列表")
    public String list(@ModelAttribute("qo") SupplierQueryObject qo , Model model)throws Exception{
        model.addAttribute("result",orderBillService.query(qo));
        model.addAttribute("suppliers",supplierService.list());
        return "orderBill/list";
    }

    @RequestMapping("input")
    @RequiredPermission("采购订单编辑")
    public String input(Long id,Model model)throws Exception{
        if(id!=null){
            model.addAttribute("entity",orderBillService.get(id));
        }
        model.addAttribute("suppliers",supplierService.list());
        return "orderBill/input";
    }
    @RequestMapping("view")
    @RequiredPermission("采购订单编辑")
    public String view(Long id,Model model)throws Exception{
        if(id!=null){
            model.addAttribute("entity",orderBillService.get(id));
        }
        model.addAttribute("suppliers",supplierService.list());
        return "orderBill/view";
    }

    @RequestMapping("delete")
    @ResponseBody
    @RequiredPermission("采购订单删除")
    public Object delete(Long id)throws Exception{
       if(id!=null){
           orderBillService.delete(id);
       }
        return new JSONResult();
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(OrderBill entity)throws Exception{
        orderBillService.saveOrUpdate(entity);
        return new JSONResult();
        //"redirect:/orderBill/list.do";
    }
}
