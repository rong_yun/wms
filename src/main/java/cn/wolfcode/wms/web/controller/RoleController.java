package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.Role;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IPermissionService;
import cn.wolfcode.wms.service.IRoleService;
import cn.wolfcode.wms.service.ISystemMenuService;
import cn.wolfcode.wms.util.JSONResult;
import cn.wolfcode.wms.util.RequiredPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("role")
public class RoleController {
    @Autowired
    private IRoleService roleService;
    @Autowired
    private IPermissionService permissionService;
    @Autowired
    private ISystemMenuService systemMenuService;

    @RequestMapping("list")
    @RequiredPermission("角色列表")
    public String list(@ModelAttribute("qo") QueryObject qo , Model model)throws Exception{
        model.addAttribute("result",roleService.query(qo));
        return "role/list";
    }

    @RequestMapping("input")
    @RequiredPermission("角色列表")
    public String input(Long id,Model model)throws Exception{
        if(id!=null){
            model.addAttribute("entity",roleService.get(id));
        }
        model.addAttribute("permissions",permissionService.list());
        model.addAttribute("menus",systemMenuService.list());
        return "role/input";
    }

    @RequestMapping("delete")
    @ResponseBody
    @RequiredPermission("角色列表")
    public Object delete(Long id)throws Exception{
       if(id!=null){
           roleService.delete(id);
       }
        return new JSONResult();
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(Role entity,Long[] ids,Long[] menuIds)throws Exception{
        roleService.saveOrUpdate(entity,ids,menuIds);
        return new JSONResult();
        //"redirect:/role/list.do";
    }
}
