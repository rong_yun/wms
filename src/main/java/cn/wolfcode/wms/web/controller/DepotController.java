package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.Depot;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IDepotService;
import cn.wolfcode.wms.util.JSONResult;
import cn.wolfcode.wms.util.RequiredPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("depot")
public class DepotController {
    @Autowired
    private IDepotService depotService;

    @RequestMapping("list")
    @RequiredPermission("部门列表")
    public String list(@ModelAttribute("qo") QueryObject qo , Model model)throws Exception{
        model.addAttribute("result",depotService.query(qo));
        return "depot/list";
    }

    @RequestMapping("input")
    @RequiredPermission("部门编辑")
    public String input(Long id,Model model)throws Exception{
        if(id!=null){
            model.addAttribute("entity",depotService.get(id));
        }
        return "depot/input";
    }

    @RequestMapping("delete")
    @ResponseBody
    @RequiredPermission("部门删除")
    public Object delete(Long id)throws Exception{
       if(id!=null){
           depotService.delete(id);
       }
        return new JSONResult();
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(Depot entity)throws Exception{
        depotService.saveOrUpdate(entity);
        return new JSONResult();
        //"redirect:/depot/list.do";
    }
}
