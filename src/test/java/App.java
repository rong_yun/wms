import cn.wolfcode.wms.domain.Department;

import cn.wolfcode.wms.service.IDepartmentService;
import cn.wolfcode.wms.service.IEmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class App {
    @Autowired
    private IDepartmentService service;
    @Autowired
    private IEmployeeService employeeService;
    @Test
    public void saveOrUpdate() {
       Department d=new Department();
       d.setName("lllll");
       d.setSn("ssss");
       service.saveOrUpdate(d);
    }

    @Test
    public void delete() {

    }

    @Test
    public void get() {
        System.out.println(employeeService.get(2L));
    }

    @Test
    public void list() {
     // employeeService.list().forEach(System.out::println);
    }
}